# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Configuration'
        db.create_table(u'eventmanager_configuration', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_time', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('publish_talk_results', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('private', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('block_submit_talk', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('block_attendees', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('block_schedule', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('block_my_talks', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('block_talk_page', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('block_call_for_proposals', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('block_review_talks', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('block_sponsors', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('block_information', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('block_register', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('block_code_of_conduct', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('block_press_release', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('block_volunteers', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('block_contact', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'eventmanager', ['Configuration'])

        # Adding model 'Event'
        db.create_table(u'eventmanager_event', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('url', self.gf('django.db.models.fields.CharField')(unique=True, max_length=150)),
            ('summary', self.gf('django.db.models.fields.CharField')(max_length=1000)),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
            ('language', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('configuration', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['eventmanager.Configuration'])),
        ))
        db.send_create_signal(u'eventmanager', ['Event'])

        # Adding M2M table for field admin on 'Event'
        m2m_table_name = db.shorten_name(u'eventmanager_event_admin')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm[u'eventmanager.event'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['event_id', 'user_id'])

        # Adding M2M table for field members on 'Event'
        m2m_table_name = db.shorten_name(u'eventmanager_event_members')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm[u'eventmanager.event'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['event_id', 'user_id'])

        # Adding model 'Mail'
        db.create_table(u'eventmanager_mail', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('last_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('body', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'eventmanager', ['Mail'])

        # Adding model 'EventMail'
        db.create_table(u'eventmanager_eventmail', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['eventmanager.Event'])),
            ('mail', self.gf('django.db.models.fields.related.ForeignKey')(related_name='first_email', to=orm['eventmanager.Mail'])),
        ))
        db.send_create_signal(u'eventmanager', ['EventMail'])

        # Adding model 'Reply'
        db.create_table(u'eventmanager_reply', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event_mail', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['eventmanager.EventMail'])),
            ('mail', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['eventmanager.Mail'])),
        ))
        db.send_create_signal(u'eventmanager', ['Reply'])

        # Adding model 'UnreadMail'
        db.create_table(u'eventmanager_unreadmail', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['eventmanager.Event'])),
            ('mail', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['eventmanager.Mail'])),
        ))
        db.send_create_signal(u'eventmanager', ['UnreadMail'])

        # Adding model 'Comments'
        db.create_table(u'eventmanager_comments', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=500)),
        ))
        db.send_create_signal(u'eventmanager', ['Comments'])

        # Adding model 'Tasks'
        db.create_table(u'eventmanager_tasks', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['eventmanager.Event'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('priority', self.gf('django.db.models.fields.IntegerField')(max_length=1)),
            ('last_update', self.gf('django.db.models.fields.DateTimeField')()),
            ('ending', self.gf('django.db.models.fields.DateField')(null=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('assigned', self.gf('django.db.models.fields.related.ForeignKey')(related_name='assigned_user', to=orm['auth.User'])),
            ('closed_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name='closed_by_user', null=True, to=orm['auth.User'])),
        ))
        db.send_create_signal(u'eventmanager', ['Tasks'])

        # Adding M2M table for field comments on 'Tasks'
        m2m_table_name = db.shorten_name(u'eventmanager_tasks_comments')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('tasks', models.ForeignKey(orm[u'eventmanager.tasks'], null=False)),
            ('comments', models.ForeignKey(orm[u'eventmanager.comments'], null=False))
        ))
        db.create_unique(m2m_table_name, ['tasks_id', 'comments_id'])


    def backwards(self, orm):
        # Deleting model 'Configuration'
        db.delete_table(u'eventmanager_configuration')

        # Deleting model 'Event'
        db.delete_table(u'eventmanager_event')

        # Removing M2M table for field admin on 'Event'
        db.delete_table(db.shorten_name(u'eventmanager_event_admin'))

        # Removing M2M table for field members on 'Event'
        db.delete_table(db.shorten_name(u'eventmanager_event_members'))

        # Deleting model 'Mail'
        db.delete_table(u'eventmanager_mail')

        # Deleting model 'EventMail'
        db.delete_table(u'eventmanager_eventmail')

        # Deleting model 'Reply'
        db.delete_table(u'eventmanager_reply')

        # Deleting model 'UnreadMail'
        db.delete_table(u'eventmanager_unreadmail')

        # Deleting model 'Comments'
        db.delete_table(u'eventmanager_comments')

        # Deleting model 'Tasks'
        db.delete_table(u'eventmanager_tasks')

        # Removing M2M table for field comments on 'Tasks'
        db.delete_table(db.shorten_name(u'eventmanager_tasks_comments'))


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'eventmanager.comments': {
            'Meta': {'object_name': 'Comments'},
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'eventmanager.configuration': {
            'Meta': {'object_name': 'Configuration'},
            'block_attendees': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_call_for_proposals': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_code_of_conduct': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block_contact': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_information': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_my_talks': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_press_release': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block_register': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_review_talks': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_schedule': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_sponsors': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_submit_talk': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_talk_page': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block_volunteers': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'first_time': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'publish_talk_results': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'eventmanager.event': {
            'Meta': {'object_name': 'Event'},
            'admin': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_admin'", 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'configuration': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.Configuration']"}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_member'", 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '1000'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'url': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        u'eventmanager.eventmail': {
            'Meta': {'object_name': 'EventMail'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'first_email'", 'to': u"orm['eventmanager.Mail']"}),
            'replies': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['eventmanager.Mail']", 'through': u"orm['eventmanager.Reply']", 'symmetrical': 'False'})
        },
        u'eventmanager.mail': {
            'Meta': {'object_name': 'Mail'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_date': ('django.db.models.fields.DateTimeField', [], {}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'eventmanager.reply': {
            'Meta': {'object_name': 'Reply'},
            'event_mail': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.EventMail']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.Mail']"})
        },
        u'eventmanager.tasks': {
            'Meta': {'object_name': 'Tasks'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'assigned': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assigned_user'", 'to': u"orm['auth.User']"}),
            'closed_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'closed_by_user'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'comments': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['eventmanager.Comments']", 'symmetrical': 'False'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'ending': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'priority': ('django.db.models.fields.IntegerField', [], {'max_length': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'eventmanager.unreadmail': {
            'Meta': {'object_name': 'UnreadMail'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.Mail']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        }
    }

    complete_apps = ['eventmanager']