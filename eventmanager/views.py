import json
import datetime
import pytz

from django.http import Http404, HttpResponse
from django.core.mail import EmailMessage
from django.db.models import Q
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.forms.models import modelformset_factory
from django.utils.translation import ugettext_lazy as _
from pyconference.common.utils import render_response

from eventmanager import blocks_configuration
from eventmanager.api import api_views
from eventmanager.models import (
    Event, Configuration, Tasks, EventMail, UnreadMail, Mail, Reply, Comments
)
from eventmanager.forms import EventSettingsForm, ConfigurationForm
from eventmanager.unicode_csv import UnicodeWriter
from blocks.attendees.models import User, Profile, Attendee
from blocks.sponsors.models import Sponsor, SponsorLevel, SponsorshipInfo
from blocks.talks.models import TalkEvent
from blocks.location.models import Location
from blocks.custom_design.models import HomeSections, SectionsOrder
from blocks.social.models import Social
from blocks.single_pages.models import SinglePage
from blocks.schedule.models import Track, Schedule
from eventmanager.api.api_views import is_valid_event, is_valid_event_and_info
from blocks.location.forms import LocationForm

import redis


@login_required
def dashboard(request):
    """Dashboard Page."""
    if "pycon.com.ar" in request.build_absolute_uri():
        return HttpResponseRedirect(reverse("show_conference",
                                    args=["pyconar2014"]))
    events = Event.objects.filter(
        Q(admin=request.user) | Q(members=request.user)
    ).distinct().order_by("-start_date")
    for event in events:
        count = Attendee.objects.filter(event=event).count()
        setattr(event, "attendees_count", count)
        talks = TalkEvent.objects.select_related('talk').filter(event=event)
        already_registered_speakers = []
        count = 0
        for talk in talks:
            if talk.talk.speaker not in already_registered_speakers:
                already_registered_speakers.append(talk.talk.speaker)
                count += 1
        # We should use this, but is not supported on sqlite, so not sure
        #count = TalkEvent.objects.select_related('talk').filter(
            #event=event).distinct("talk__speaker").count()
        setattr(event, "speakers_count", count)
        count = Sponsor.objects.filter(event=event).count()
        setattr(event, "sponsors_count", count)
    pending_tasks = []
    for event in events:
        all_tasks = Tasks.objects.filter(event=event).count()
        open_tasks = Tasks.objects.filter(
            event=event, active=True).count()
        today = datetime.datetime.now(pytz.timezone(str(event.timezone)))
        delta_time = event.start_date - today
        message = "%d tasks to do / %d days left" % (
            open_tasks, delta_time.days)
        link = reverse("tasks", args=[event.id])
        percentage = 0
        if all_tasks > 0:
            percentage = 100 - ((open_tasks * 100) / all_tasks)
            pending_tasks.append({
                'message': message,
                'event_name': event.title,
                'link': link,
                'percentage': str(percentage) + "%"
            })
        else:
            pending_tasks.append({
                'message': message,
                'event_name': event.title,
                'link': link,
                'percentage': str(100) + "%"
            })
    feeds = []
    tasks_db = Tasks.objects.filter(
        Q(creator=request.user) | Q(assigned=request.user) |
        Q(assigned=request.user)).order_by("-last_update")[:10]
    for task in tasks_db:
        if task.active:
            last_comment = task.comments.all().order_by(
                "-date")[0]
            feeds.append({
                "taskid": task.id,
                "active": True,
                "user": last_comment.user.username,
                "link_user": reverse("profile",
                                     args=['dashboard',
                                           last_comment.user.username]),
                "title": task.title,
                "link_task": reverse("tasks", args=[task.event.id]),
                "time": last_comment.date.date().strftime('%d, %b %Y'),
            })
        else:
            feeds.append({
                "taskid": task.id,
                "user": task.closed_by.username,
                "link_user": reverse("profile",
                                     args=['dashboard',
                                           task.closed_by.username]),
                "title": task.title,
                "link_task": reverse("tasks", args=[task.event.id]),
                "time": task.last_update.date().strftime('%d, %b %Y'),
            })
    attending = []
    attend_db = Attendee.objects.select_related("Event").filter(
        profile__user=request.user)
    for attend in attend_db:
        today = datetime.datetime.now(pytz.timezone(str(event.timezone)))
        delta_time = attend.event.start_date - today
        attending.append({
            "title": attend.event.title,
            "url": attend.event.url,
            "date": attend.event.start_date.strftime('%d, %b %Y'),
            "days_left": delta_time.days,
        })
    data = {'dashboard_active': 'active',
            'username': request.user.username,
            'events': events,
            'attending': attending,
            'feeds': feeds,
            'dashboard': True,
            'pending_tasks': pending_tasks}
    return render_response(request, 'dashboard/dashboard.html', data)


@login_required
def create_conference(request):
    """Create conference."""
    configuration = blocks_configuration.get_default_configuration()
    data = {"display": "none", "username": request.user.username,
            "dashboard": True, "configuration": configuration}
    if request.method == "POST":
        settings_form = EventSettingsForm(request.POST,
                                          request.FILES, prefix="event")
        blocks_form = ConfigurationForm(request.POST, prefix="configuration")
        if settings_form.is_valid() and blocks_form.is_valid():
            configuration = blocks_form.save()
            event = settings_form.save(commit=False)
            event.configuration = configuration
            event.save()
            settings_form.save_m2m()
            event.admin.add(request.user)
            profile, _ = Profile.objects.get_or_create(user=request.user)
            attendee, _ = Attendee.objects.get_or_create(
                profile=profile, event=event)
            return HttpResponseRedirect(reverse("event_manager",
                                                args=[event.id]))
    else:
        settings_form = EventSettingsForm(prefix="event")
        blocks_form = ConfigurationForm(prefix="configuration")
    data['settings_form'] = settings_form
    data['blocks_form'] = blocks_form
    return render_response(request, 'create_conference.html', data)


@login_required
@is_valid_event_and_info
def event_manager(request, eventid="0", **kw):
    """Event Manager."""
    if "pycon.com.ar" in request.build_absolute_uri():
        return HttpResponseRedirect(reverse("show_conference",
                                    args=["pyconar2014"]))
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    members_counter = (event.admin.all().count() +
                       event.members.all().count())
    feeds = []
    tasks_db = Tasks.objects.filter(
        Q(creator=request.user) | Q(assigned=request.user) |
        Q(assigned=request.user), event=event).order_by("-last_update")[:10]
    for task in tasks_db:
        if task.active:
            last_comment = task.comments.all().order_by(
                "-date").first()
            feeds.append({
                "taskid": task.id,
                "active": True,
                "user": last_comment.user.username,
                "title": task.title,
                "link_user": reverse("profile",
                                     args=['dashboard',
                                           last_comment.user.username]),
                "link_task": reverse("tasks", args=[task.event.id]),
                "time": last_comment.date.date().strftime('%d, %b %Y'),
            })
        else:
            feeds.append({
                "taskid": task.id,
                "user": task.closed_by.username,
                "title": task.title,
                "link_user": reverse("profile",
                                     args=['dashboard',
                                           task.closed_by.username]),
                "link_task": reverse("tasks", args=[task.event.id]),
                "time": task.last_update.date().strftime('%d, %b %Y'),
            })
    configuration = blocks_configuration.get_configurations(event)
    data = {
        'project_name': event.title,
        'event_manager_active': 'active',
        'url': event.url,
        'project_description': event.summary,
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'members_counter': members_counter,
        "username": request.user.username,
        'feeds': feeds,
        'eventid': eventid,
        'configuration': configuration}
    if event.configuration.first_time:
        data["first_time"] = True
        event.configuration.first_time = False
        event.configuration.save()
    return render_response(request, 'dashboard/event_manager.html', data)


@login_required
@is_valid_event_and_info
def inbox(request, eventid="0", **kw):
    """Event Manager - Inbox."""
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    event_mails = EventMail.objects.select_related("Mail").filter(
        event=event).order_by("-mail__last_date")
    count = event_mails.count()
    mails = []
    unread_mails = UnreadMail.objects.filter(
        event=event, user=request.user).values_list(
        "mail__id", flat=True)
    for eventmail in event_mails[0:10]:
        username = eventmail.mail.user.username
        replies = eventmail.replies.all().order_by("-date")
        if replies[:1]:
            username = "%s .. %s (%d)" % (
                username, replies[0].user.username, replies.count() + 1)
        mails.append({
            "id": eventmail.mail.id,
            "username": username,
            "subject": eventmail.mail.subject,
            "date": eventmail.mail.date.strftime('%d, %b %Y'),
            "unread": eventmail.mail.id in unread_mails
        })
    page_items = 10 if count >= 10 else count
    data = {
        'project_name': event.title,
        'inbox_active': 'active',
        'unread': 0,
        'page': "%d-%d" % (0, page_items),
        'pagenum': count,
        'mails': mails,
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        "username": request.user.username,
        'eventid': eventid}
    return render_response(request, 'dashboard/inbox.html', data)


@login_required
@is_valid_event_and_info
def statistics(request, eventid="0", **kw):
    """Event Manager - Statistics."""
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    data = {
        'project_name': event.title,
        'statistics_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'eventid': event.id,
        "username": request.user.username,
        'eventid': eventid}
    return render_response(request, 'dashboard/statistics.html', data)


@login_required
@is_valid_event_and_info
def remove_contact(request, eventid="0", **kw):
    event = kw["event"]
    remove = request.POST.get('remove', "")
    operation = request.POST.get('operation', "")
    is_admin = request.user in event.admin.all()
    if is_admin:
        if operation == "member":
            user = User.objects.filter(username=remove).first()
            if user in event.members.all():
                event.members.remove(user)
            elif user in event.admin.all() and user == request.user:
                event.admin.remove(request.user)
                return HttpResponseRedirect(reverse("dashboard"))
        elif operation == "sponsor":
            sponsor = Sponsor.objects.filter(
                name=remove, event=event).first()
            if sponsor:
                sponsor.delete()
    return HttpResponseRedirect(reverse("contacts", args=[event.id]))


@login_required
@is_valid_event_and_info
def add_sponsor(request, eventid="0", **kw):
    event = kw["event"]
    name = request.POST.get('name', "")
    email = request.POST.get('email', "")
    phone = request.POST.get('phone', "")
    website = request.POST.get('website', "")
    description = request.POST.get('description', "")
    if name and (email or phone):
        sponsor, _ = Sponsor.objects.get_or_create(
            event=event, name=name)
        sponsor.email = email
        sponsor.phone = phone
        sponsor.company_page = website
        sponsor.description = description
        sponsor.save()
    return HttpResponseRedirect(reverse("contacts", args=[event.id]))


@login_required
@is_valid_event_and_info
def contacts(request, eventid="0", **kw):
    """Event Manager - Contacts."""
    event = kw["event"]
    is_admin = request.user in event.admin.all()
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    current = "%s_active" % request.GET.get("current", "members")
    members = []
    for user in event.admin.all():
        profile, _ = Profile.objects.get_or_create(user=user)
        members.append({
            "username": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "webpage": profile.personal_page if profile.personal_page else "",
            "phone": profile.phone if profile.phone else ""
        })
    for user in event.members.all():
        profile, _ = Profile.objects.get_or_create(user=user)
        members.append({
            "username": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "webpage": profile.personal_page if profile.personal_page else "",
            "phone": profile.phone if profile.phone else ""
        })
    sponsors = []
    sponsors_db = Sponsor.objects.filter(event=event)
    for sponsor in sponsors_db:
        sponsors.append({
            "name": sponsor.name,
            "email": sponsor.email,
            "webpage": sponsor.company_page,
            "phone": sponsor.phone
        })
    attendees = []
    attendees_db = Attendee.objects.filter(event=event)
    for attendee in attendees_db:
        attendees.append({
            "first_name": attendee.profile.user.first_name,
            "last_name": attendee.profile.user.last_name,
            "email": attendee.profile.user.email,
            "webpage": (attendee.profile.personal_page
                        if attendee.profile.personal_page else "")
        })
    speakers = []
    talks = TalkEvent.objects.select_related('talk').filter(event=event)
    already_registered_speakers = []
    for talk in talks:
        profile, _ = Profile.objects.get_or_create(user=talk.talk.speaker)
        if talk.talk.speaker not in already_registered_speakers:
            already_registered_speakers.append(talk.talk.speaker)
            speakers.append({
                "first_name": profile.user.first_name,
                "last_name": profile.user.last_name,
                "email": profile.user.email,
                "webpage": (profile.personal_page
                            if profile.personal_page else ""),
                "telephone": profile.phone if profile.phone else ""
            })

    sponsorship_url = SponsorshipInfo.objects.filter(event=event).first()
    sponsorship_url = (
        sponsorship_url.url if sponsorship_url and sponsorship_url.url else "")

    data = {
        'project_name': event.title,
        'contacts_active': 'active',
        'eventid': eventid,
        'members': members,
        'attendees': attendees,
        'speakers': speakers,
        'unread': unread,
        'tasks': tasks,
        'is_admin': is_admin,
        'messages': messages,
        'newer_tasks': newer_tasks,
        "username": request.user.username,
        'sponsors': sponsors,
        'sponsorship_url': sponsorship_url,
        current: 'active'}
    return render_response(request, 'dashboard/contacts.html', data)


@login_required
@is_valid_event_and_info
def contacts_export(request, eventid="0", **kw):
    event = kw["event"]

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="attendees.csv"'

    writer = UnicodeWriter(response)
    writer.writerow([
        'first_name',
        'last_name',
        'email',
        'country',
        'state',
        'phone',
        'personal_page',
        'company',
        'company_page',
        'cv_link',
        'cv_uploaded',
        'allow_sponsors_contact',
        'biography',
    ])

    for attendee in Attendee.objects.filter(event=event):
        writer.writerow([
            attendee.profile.user.first_name,
            attendee.profile.user.last_name,
            attendee.profile.user.email,
            unicode(attendee.profile.country) or "",
            attendee.profile.state or "",
            attendee.profile.phone or "",
            attendee.profile.personal_page or "",
            attendee.profile.company or "",
            attendee.profile.company_page or "",
            attendee.profile.cv_link or "",
            attendee.profile.cv.url if attendee.profile.cv else "",
            "YES" if attendee.profile.allow_contact else "NO",
            attendee.profile.biography or "",
        ])

    return response


def sponsorship_info(request, eventid="0", infouuid=""):
    """Event Manager - Contacts."""
    valid_request = True
    event = None
    try:
        event = Event.objects.filter(id=int(eventid)).first()
    except Event.DoesNotExist:
        valid_request = False
    if not valid_request or event is None:
        return render_response(request, 'error404.html')
    attendees = []
    attendees_db = Attendee.objects.filter(
        event=event, profile__allow_contact=True)
    for attendee in attendees_db:
        attendees.append({
            "username": attendee.profile.user.username,
            "first_name": attendee.profile.user.first_name,
            "last_name": attendee.profile.user.last_name,
            "email": attendee.profile.user.email,
            "company": (attendee.profile.company
                        if attendee.profile.company else ""),
            "company_page": attendee.profile.company_page,
            "cv_link": (attendee.profile.cv_link
                        if attendee.profile.cv_link else ""),
            "webpage": (attendee.profile.personal_page
                        if attendee.profile.personal_page else "")
        })

    data = {
        'project_name': event.title,
        'eventid': eventid,
        'url': event.url,
        'attendees': attendees}
    return render_response(
        request, 'dashboard/sponsors_attendees_info.html', data)


@login_required
@is_valid_event_and_info
def designer(request, eventid="0", **kw):
    """Event Manager - Designer."""
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    data = {
        'project_name': event.title,
        'ui_designer_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'eventid': event.id,
        "username": request.user.username,
        'eventid': eventid}
    return render_response(request, 'dashboard/designer.html', data)


@login_required
@is_valid_event_and_info
def tasks(request, eventid="0", **kw):
    """Event Manager - Tasks."""
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    taskid = request.GET.get("id", "")
    members = sorted(tuple(event.admin.all()) + tuple(event.members.all()))
    tasks_db = Tasks.objects.filter(
        event=event, active=True).order_by("priority", "-last_update")
    count = tasks_db.count()
    tasks_list = []
    for task in tasks_db[:10]:
        if task.ending:
            date = task.ending.strftime('%d, %b %Y')
            tasks_list.append({
                "id": task.id,
                "priority": task.priority,
                "date": date,
                "title": task.title,
            })
        else:
            tasks_list.append({
                "id": task.id,
                "priority": task.priority,
                "title": task.title,
            })
    assigned_to_you = Tasks.objects.filter(
        event=event, active=True, assigned=request.user).count()
    created_by_you = Tasks.objects.filter(
        event=event, active=True, creator=request.user).count()
    page_items = 10 if count >= 10 else count
    data = {
        'project_name': event.title,
        'tasks_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'page': "%d-%d" % (0, page_items),
        'pagenum': count,
        'tasks_list': tasks_list,
        'current_user': request.user.username,
        'assigned_to_you': assigned_to_you,
        'created_by_you': created_by_you,
        'members': members,
        "username": request.user.username,
        'eventid': eventid}
    if taskid:
        data["taskid"] = taskid
    return render_response(request, 'dashboard/tasks.html', data)


@login_required
@is_valid_event_and_info
def calendar(request, eventid="0", **kw):
    """Event Manager - Calendar."""
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    members = sorted(tuple(event.admin.all()) + tuple(event.members.all()))
    tasks_db = Tasks.objects.filter(
        event=event, active=True,
        ending=None).order_by("priority", "-last_update")
    tasks_list = []
    for task in tasks_db[:10]:
        tasks_list.append({
            "id": task.id,
            "priority": task.priority,
            "title": task.title,
        })
    data = {
        'project_name': event.title,
        'calendar_active': 'active',
        'tasks_list': tasks_list,
        'unread': unread,
        'members': members,
        'current_user': request.user.username,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        "username": request.user.username,
        'eventid': eventid}
    return render_response(request, 'dashboard/calendar.html', data)


@login_required
@is_valid_event_and_info
def schedule(request, eventid="0", **kw):
    """Event Manager - Calendar."""
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    track_db = Track.objects.filter(event=event).order_by("name")
    schedule_db = Schedule.objects.filter(event=event).values_list(
        "talk", flat=True)
    talk_event_db = TalkEvent.objects.filter(event=event, accepted=True)
    talks = []
    for talk in talk_event_db:
        if talk.id not in schedule_db:
            talks.append(talk)
    data = {
        'project_name': event.title,
        'schedule_active': 'active',
        'unread': unread,
        'current_user': request.user.username,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        "username": request.user.username,
        'tracks': track_db,
        'talks': talks,
        'event_start_year': event.start_date.year,
        'event_start_month': event.start_date.month,
        'event_start_day': event.start_date.day,
        'eventid': eventid}
    return render_response(request, 'dashboard/schedule.html', data)


@login_required
@is_valid_event_and_info
def invoice(request, eventid="0", **kw):
    """Event Manager - Invoice."""
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    data = {
        'project_name': event.title,
        'invoice_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'eventid': event.id,
        "username": request.user.username,
        'eventid': eventid}
    return render_response(request, 'dashboard/invoice.html', data)


@login_required
@is_valid_event_and_info
def social(request, eventid="0", **kw):
    """Event Manager - Social."""
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    social, _ = Social.objects.get_or_create(event=event)
    data = {
        'project_name': event.title,
        'social_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'eventid': event.id,
        "username": request.user.username,
        'eventid': eventid,
        'twitter_link': social.twitter_link,
        'facebook_link': social.facebook_link,
        'google_link': social.google_link,
        'pinterest_link': social.pinterest_link}
    return render_response(request, 'dashboard/social.html', data)


@login_required
@is_valid_event_and_info
def location(request, eventid="0", **kw):
    """Event Manager - Location."""
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    form = LocationForm()
    data = {
        'project_name': event.title,
        'location_active': 'active',
        'unread': unread,
        'eventid': event.id,
        "username": request.user.username,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'form': form,
        'eventid': eventid}
    return render_response(request, 'dashboard/location.html', data)


@login_required
@is_valid_event_and_info
def settings(request, eventid="0", **kw):
    """Settings page."""
    event = kw["event"]
    is_admin = request.user in event.admin.all()
    if request.method == "POST":
        form = EventSettingsForm(request.POST, request.FILES,
                                instance=event)
        if form.is_valid() and is_admin:
            form.save()
            return HttpResponseRedirect(
                reverse("event_manager", args=[event.id]))
    else:
        form = EventSettingsForm(instance=event)
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    newer_tasks = kw["newer_tasks"]
    data = {
        'project_name': event.title,
        'url': event.url,
        'is_admin': is_admin,
        'start_date': event.start_date.strftime('%d/%m/%Y'),
        'end_date': event.end_date.strftime('%d/%m/%Y'),
        'summary': event.summary,
        'tags': ",".join(event.tags.names()),
        'settings_active': 'active',
        'form': form,
        'unread': unread,
        'eventid': event.id,
        "username": request.user.username,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'eventid': eventid }
    return render_response(request, 'settings.html', data)


@login_required
@is_valid_event
def delete_conference(request, eventid="0", event=None):
    """Delete conference."""
    is_admin = request.user in event.admin.all()
    if is_admin and event.title == request.POST.get("conferenceName"):
        event.delete()
    elif event.title == request.POST.get("conferenceName"):
        event.members.remove(request.user)
    else:
        return HttpResponseRedirect(reverse("event_manager",
                                    args=[event.id]))
    return HttpResponseRedirect(reverse("dashboard"))


@is_valid_event
def send_mail(request, eventid, event):
    subject = request.POST.get("subject", "")
    body = request.POST.get("body", "")
    if subject and body:
        mail = Mail(
            user=request.user,
            date=datetime.datetime.now(),
            last_date=datetime.datetime.now(),
            subject=subject,
            body=body)
        mail.save()
        event_mail = EventMail(
            event=event,
            mail=mail)
        event_mail.save()
        notify_users = []
        notify_emails = []
        for admin in event.admin.all():
            if admin != request.user:
                unread = UnreadMail(user=admin, event=event, mail=mail)
                unread.save()
                notify_users.append(admin.username)
                notify_emails.append(admin.email)
        for member in event.members.all():
            if member != request.user:
                unread = UnreadMail(user=member, event=event, mail=mail)
                unread.save()
                notify_users.append(member.username)
                notify_emails.append(member.email)
        # Once mail has been created, notify other users
        link = reverse("inbox", args=[event.id])
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        r.publish('mails',
                  json.dumps({"data": {'user': request.user.username,
                              'subject': subject, 'link': link},
                              'notify_users': notify_users}))
        body_email = "Message at: http://myconference.co/%s\n\n%s" % (
            reverse("inbox", args=[event.id]), body)
        email = EmailMessage(subject, body_email, to=notify_emails)
        email.send()
    return HttpResponseRedirect(reverse("inbox", args=[event.id]))


@is_valid_event
def reply_mail(request, eventid, event):
    mail_id = int(request.POST.get("mailId", ""))
    body = request.POST.get("body", "")
    base_mail = Mail.objects.filter(id=mail_id).first()
    if base_mail and body:
        event_mail = EventMail.objects.filter(
            event=event, mail=base_mail).first()
        if event_mail:
            mail = Mail(
                user=request.user,
                date=datetime.datetime.now(),
                last_date=datetime.datetime.now(),
                body=body)
            mail.save()
            base_mail.last_date = datetime.datetime.now()
            base_mail.save()
            reply = Reply(event_mail=event_mail, mail=mail)
            reply.save()
            notify_users = []
            for admin in event.admin.all():
                if admin != request.user:
                    unread = UnreadMail(
                        user=admin, event=event, mail=mail)
                    unread.save()
                    unread = UnreadMail(
                        user=admin, event=event, mail=base_mail)
                    unread.save()
                    notify_users.append(admin.username)
            for member in event.members.all():
                if member != request.user:
                    unread = UnreadMail(
                        user=member, event=event, mail=mail)
                    unread.save()
                    unread = UnreadMail(
                        user=member, event=event, mail=base_mail)
                    unread.save()
                    notify_users.append(member.username)
        # Once mail has been created, notify other users
        link = reverse("inbox", args=[event.id])
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        r.publish('mails',
                  json.dumps({"data": {'user': request.user.username,
                              'subject': mail.subject, 'link': link},
                              'notify_users': notify_users}))
    return HttpResponseRedirect(reverse("inbox", args=[event.id]))


@is_valid_event
def create_task(request, eventid, event):
    taskid = request.POST.get("taskId", "")
    title = request.POST.get("taskTitleInput", "")
    body = request.POST.get("body", "")
    priority = request.POST.get("priority", "")
    ending = request.POST.get("endingOn", "")
    assigned = request.POST.get("assignedTo", "")
    assigned_user = User.objects.filter(username=assigned).first()
    try:
        if ending and isinstance(ending, basestring):
            date = [int(d) for d in ending.split("/")]
            ending = datetime.date(date[2], date[1], date[0])
        else:
            ending = None
    except:
        print "not ending assigned"
    if title and assigned_user:
        if taskid == "":
            priority = Tasks.MAP_CHOICES.get(
                priority, Tasks.PR_HIGH)
            task = Tasks(
                creator=request.user, event=event, title=title,
                priority=priority, last_update=datetime.datetime.now(),
                active=True, assigned=assigned_user, ending=ending)
            task.save()
            comment = Comments(
                user=request.user, date=datetime.datetime.now(),
                comment=body)
            comment.save()
            task.comments.add(comment)
        else:
            taskid = int(taskid)
            task = Tasks.objects.filter(id=taskid).first()
            if task:
                priority = Tasks.MAP_CHOICES.get(
                    priority, Tasks.PR_HIGH)
                task.priority = priority
                task.last_update = datetime.datetime.now()
                task.assigned = assigned_user
                task.ending = task.ending
                task.save()
                comment = Comments(
                    user=request.user, date=datetime.datetime.now(),
                    comment=body)
                comment.save()
                task.comments.add(comment)
        # Once mail has been created, notify other users
        link = reverse("tasks", args=[event.id])
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        r.publish('tasks',
                  json.dumps({"data": {'priority': task.priority,
                                       'title': task.title, 'link': link,
                                       'user': request.user.username},
                              'notify_users': [assigned_user.username]}))
    return HttpResponseRedirect(reverse("tasks", args=[event.id]))


@login_required
@is_valid_event_and_info
def call_for_proposals(request, eventid="0", **kw):
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    category = "call_for_proposals"
    data = {
        'project_name': event.title,
        'statistics_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'eventid': event.id,
        "username": request.user.username,
        "single_page_title": _("Call for Proposals"),
        "category": category,
        'eventid': eventid}
    if request.method == 'GET':
        page = SinglePage.objects.filter(event=event, category=category).first()
    else:
        text = request.POST.get("htmlText", "")
        page, created = SinglePage.objects.get_or_create(
            event=event, category=category)
        if page:
            page.text = text
            page.save()
    if page:
        data["content_page"] = page.text
    return render_response(
        request, 'dashboard/single_page_config.html', data)


@login_required
@is_valid_event_and_info
def customize_home(request, eventid="0", **kw):
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    data = {
        'project_name': event.title,
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'eventid': event.id,
        "username": request.user.username,
        'eventid': eventid}
    sections = None
    if request.method == 'POST':
        sectionid = request.POST.get("sectionid", "")
        title = request.POST.get("title", "")
        text = request.POST.get("htmlText", "")
        if sectionid != "":
            section, created = HomeSections.objects.get_or_create(
                id=sectionid, event=event)
            if section:
                section.title = title
                section.text = text
                section.save()
        else:
            section, created = HomeSections.objects.get_or_create(
                event=event, title=title, text=text)
    sections = HomeSections.objects.filter(event=event)
    sections_order = SectionsOrder.objects.filter(event=event).first()
    orders = []
    if sections_order:
        sort_by = sections_order.order.split(",")
        for sectionid in sort_by:
            for sect in sections:
                if sectionid != "" and sect.id == int(sectionid):
                    orders.append(sect)
                    break
    else:
        for sect in sections:
            orders.append(sect)

    for sect in sections:
        if sect not in orders:
            orders.append(sect)
    if orders:
        data["sections"] = orders
    return render_response(
        request, 'dashboard/customize_home.html', data)


@login_required
@is_valid_event_and_info
def code_of_conduct(request, eventid="0", **kw):
    event = kw["event"]
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    category = "code_of_conduct"
    data = {
        'project_name': event.title,
        'statistics_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'messages': messages,
        'newer_tasks': newer_tasks,
        'eventid': event.id,
        "username": request.user.username,
        "single_page_title": _("Code of Conduct"),
        "category": category,
        'eventid': eventid}
    if request.method == 'GET':
        page = SinglePage.objects.filter(event=event, category=category).first()
    else:
        text = request.POST.get("htmlText", "")
        page, created = SinglePage.objects.get_or_create(
            event=event, category=category)
        if page:
            page.text = text
            page.save()
    if page:
        data["content_page"] = page.text
    return render_response(
        request, 'dashboard/single_page_config.html', data)


@login_required
@is_valid_event_and_info
def contact_message(request, eventid="0", **kw):
    if request.method == 'POST':
        valid, event = api_views.event_checker(request, eventid)
        if valid:
            subject = request.POST["subject"]
            body = "From: %s\n\n%s" % (request.user.email, request.POST["body"])
            mails_admins = []
            for admin in event.admin.all():
                mails_admins.append(admin.email)
            email = EmailMessage(subject, body, to=mails_admins)
            email.send()
            #send_mail(subject, body, request.user.email,
                      #mails_admins, fail_silently=False)
            return HttpResponseRedirect(reverse('show_conference',
                                                args=[event.url]))
        else:
            raise Http404
