from django.db import models
from django.dispatch import receiver
from django.contrib.auth.models import User
from timezone_field import TimeZoneField
from taggit.managers import TaggableManager
from django.utils.translation import ugettext_lazy as _


LANGUAGE_CHOICES = (
    ('en', _('English')),
    ('es', _('Spanish')),
)


class Configuration(models.Model):
    first_time = models.BooleanField(default=True)
    publish_talk_results = models.BooleanField(default=False)
    site_template = models.CharField(max_length=50, default="design02")
    # BLOCKS
    private = models.BooleanField(default=False)
    block_submit_talk = models.BooleanField(default=True)
    block_attendees = models.BooleanField(default=True)
    block_schedule = models.BooleanField(default=True)
    block_my_talks = models.BooleanField(default=True)
    block_talk_page = models.BooleanField(default=False)
    block_call_for_proposals = models.BooleanField(default=True)
    block_review_talks = models.BooleanField(default=True)
    block_sponsors = models.BooleanField(default=True)
    block_information = models.BooleanField(default=True)
    block_register = models.BooleanField(default=True)
    block_code_of_conduct = models.BooleanField(default=False)
    block_press_release = models.BooleanField(default=False)
    block_volunteers = models.BooleanField(default=True)
    block_contact = models.BooleanField(default=True)

    attributes_names = [
        "private",
        "block_submit_talk",
        "block_attendees",
        "block_schedule",
        "block_my_talks",
        "block_talk_page",
        "block_call_for_proposals",
        "block_review_talks",
        "block_sponsors",
        "block_information",
        "block_register",
        "block_code_of_conduct",
        "block_press_release",
        "block_volunteers",
        "block_contact",
    ]


class Event(models.Model):
    title = models.CharField(max_length=50,
                            verbose_name=_('Conference name'))
    url = models.SlugField(max_length=150, unique=True,
                            verbose_name='http://myconference.co/',
                            help_text=_("URL can contain letters, numbers, underscores \
                                        or hyphens, without spaces."))
    logo = models.ImageField(upload_to='images/conferenceslogos/',
                            blank=True, null=True)
    tags = TaggableManager()
    summary = models.CharField(max_length=1000,
                                verbose_name=_('Description'))
    start_date = models.DateTimeField(verbose_name=_('Start date and time'))
    end_date = models.DateTimeField(verbose_name=_('End date and time'))
    timezone = TimeZoneField(default='UTC')
    admin = models.ManyToManyField(User, related_name='user_admin')
    members = models.ManyToManyField(User, related_name='user_member')
    reviewers = models.ManyToManyField(User, related_name='user_reviewer')
    language = models.CharField(max_length=2, choices=LANGUAGE_CHOICES)
    configuration = models.ForeignKey(Configuration)


class Mail(models.Model):
    user = models.ForeignKey(User)
    date = models.DateTimeField()
    last_date = models.DateTimeField()
    subject = models.CharField(max_length=200)
    body = models.TextField()


class EventMail(models.Model):
    event = models.ForeignKey(Event)
    mail = models.ForeignKey(Mail, related_name="first_email")
    replies = models.ManyToManyField(Mail, through='Reply')


class Reply(models.Model):
    event_mail = models.ForeignKey(EventMail)
    mail = models.ForeignKey(Mail)


class UnreadMail(models.Model):
    user = models.ForeignKey(User)
    event = models.ForeignKey(Event)
    mail = models.ForeignKey(Mail)


class Comments(models.Model):
    user = models.ForeignKey(User)
    date = models.DateTimeField()
    comment = models.CharField(max_length=500)


class Tasks(models.Model):
    PR_HIGH = 1
    PR_MEDIUM = 2
    PR_LOW = 3
    TYPE_CHOICES = (
        (PR_HIGH, 'High'),
        (PR_MEDIUM, 'Medium'),
        (PR_LOW, 'Low'),
    )
    MAP_CHOICES = {
        "high": PR_HIGH,
        "medium": PR_MEDIUM,
        "low": PR_LOW
    }
    creator = models.ForeignKey(User)
    event = models.ForeignKey(Event)
    title = models.CharField(max_length=200)
    priority = models.IntegerField(max_length=1, choices=TYPE_CHOICES)
    last_update = models.DateTimeField()
    ending = models.DateField(null=True)
    active = models.BooleanField(default=True)
    assigned = models.ForeignKey(User, related_name="assigned_user")
    closed_by = models.ForeignKey(User, related_name="closed_by_user",
                                  null=True)
    comments = models.ManyToManyField(Comments)


@receiver(models.signals.post_delete, sender=Event)
def handle_deleted_event(sender, instance, **kwargs):
    if instance.configuration:
        instance.configuration.delete()
