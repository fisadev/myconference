import re
import datetime
import time
import json
import uuid

from django.db.models import Q
from django.core.urlresolvers import reverse
from rest_framework.decorators import api_view
from rest_framework.response import Response

from rest_framework.views import APIView
from taggit.models import Tag

from eventmanager.models import (
    EventMail, UnreadMail, Event, Tasks, Comments
)
from blocks.talks.models import Talk, TalkEvent
from blocks.attendees.models import User, Attendee, Profile
from blocks.social.models import Social
from blocks.location.models import Location
from blocks.single_pages.models import SinglePage
from blocks.custom_design.models import HomeSections, SectionsOrder
from blocks.sponsors.models import SponsorshipInfo
from blocks.schedule.models import Track, Schedule
from eventmanager.api.serializers import (
    TalkSerializer  # , LocationSerializer
)
from pyconference.common.utils import render_response

import redis


def event_checker(request, eventid):
    valid_request = True
    event = None
    try:
        event = Event.objects.filter(
            Q(admin=request.user) | Q(members=request.user),
            id=int(eventid)).first()
    except Event.DoesNotExist:
        valid_request = False
    return valid_request, event


def is_valid_event(func):
    def checker(request, eventid):
        valid_request = True
        event = None
        try:
            event = Event.objects.filter(
                Q(admin=request.user) | Q(members=request.user),
                id=int(eventid)).first()
        except Event.DoesNotExist:
            valid_request = False
        if not valid_request:
            return render_response(request, 'error404.html')
        else:
            return func(request, eventid, event)
    return checker


def is_valid_event_and_info(func):
    def checker(request, eventid, **kw):
        valid_request = True
        event = None
        try:
            event = Event.objects.filter(
                Q(admin=request.user) | Q(members=request.user),
                id=int(eventid)).first()
        except Event.DoesNotExist:
            valid_request = False
        if not valid_request:
            return render_response(request, 'error404.html')
        else:
            messages = UnreadMail.objects.filter(
                user=request.user, event=event).order_by(
                "-mail__last_date")
            unread = messages.count()
            messages = messages[0:3]
            newer_tasks = Tasks.objects.filter(
                event=event, active=True).order_by(
                "priority", "-last_update")
            tasks = newer_tasks.count()
            newer_tasks = newer_tasks[0:5]
            kw["event"] = event
            kw["unread"] = unread
            kw["tasks"] = tasks
            kw["messages"] = messages
            kw["newer_tasks"] = newer_tasks
            return func(request, eventid, **kw)
    return checker


@api_view(['GET'])
def valid_conference_url(request, format=None):
    eventurl = request.GET.get('eventurl', "")
    response = {"available": check_url_available(eventurl)}
    return Response(response)


def check_url_available(url):
    response = True
    pat = re.compile(r"^((\w)+(-(\w)+)*)+$")
    if url in ("", "dashboard", "profile") or not pat.match(url):
        response = False
    else:
        results = Event.objects.filter(url=url)
        if results.count() > 0:
            response = False
    return response


@api_view(['GET'])
def add_location(request, format=None):
    eventid = int(request.GET.get('eventid', ""))
    valid, event = event_checker(request, eventid)
    if valid:
        lat = request.GET.get('lat', "")
        lng = request.GET.get('lng', "")
        description = request.GET.get('description', "")
        smoking = request.GET.get('smoking', "false") == "true"
        eating = request.GET.get('eating', "false") == "true"
        drinking = request.GET.get('drinking', "false") == "true"
        toilets = request.GET.get('toilets', "false") == "true"
        location, created = Location.objects.get_or_create(
            event=event, address_lat=lat, address_lng=lng,
            more_info=description, smoking_allowed=smoking,
            eating_allowed=eating, drinking_allowed=drinking, toilets=toilets)
        if created:
            return Response({"created": True})
        else:
            return Response({"created": False})
    else:
        return Response({"created": False})


@api_view(['GET'])
def get_usernames(request, format=None):
    users = User.objects.values_list(
        "username", flat=True).order_by("username")
    return Response(users)


@api_view(['GET'])
def add_reviewer(request, eventid, format=None):
    username = request.GET.get("username", "")
    valid, event = event_checker(request, eventid)
    user = User.objects.filter(username=username).first()
    if user and user not in event.members.all():
        event.reviewers.add(user)
        profile, _ = Profile.objects.get_or_create(user=user)
        attendee, _ = Attendee.objects.get_or_create(
            profile=profile, event=event)
        data = {
            "username": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "webpage": profile.personal_page if profile.personal_page else "",
            "telephone": profile.phone if profile.phone else ""
        }
        return Response(data)
    return Response({})


@api_view(['GET'])
def add_member(request, eventid, format=None):
    username = request.GET.get("username", "")
    valid, event = event_checker(request, eventid)
    user = User.objects.filter(username=username).first()
    if user and user not in event.members.all():
        event.members.add(user)
        profile, _ = Profile.objects.get_or_create(user=user)
        attendee, _ = Attendee.objects.get_or_create(
            profile=profile, event=event)
        data = {
            "username": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "webpage": profile.personal_page if profile.personal_page else "",
            "telephone": profile.phone if profile.phone else ""
        }
        return Response(data)
    return Response({})


@api_view(['GET'])
def add_admin(request, eventid, format=None):
    username = request.GET.get("username", "")
    valid, event = event_checker(request, eventid)
    is_admin = request.user in event.admin.all()
    user = User.objects.filter(username=username).first()
    if user and is_admin and user not in event.admin.all():
        if user in event.members.all():
            event.members.remove(user)
        event.admin.add(user)
        profile, _ = Profile.objects.get_or_create(user=user)
        attendee, _ = Attendee.objects.get_or_create(
            profile=profile, event=event)
        data = {
            "username": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "webpage": profile.personal_page if profile.personal_page else "",
            "telephone": profile.phone if profile.phone else ""
        }
        return Response(data)
    return Response({})


@api_view(['GET'])
def sponsors_info_generate(request, eventid, format=None):
    valid, event = event_checker(request, eventid)
    is_admin = request.user in event.admin.all()
    if valid and is_admin:
        info, _ = SponsorshipInfo.objects.get_or_create(event=event)
        info.url = str(uuid.uuid4()).replace("-", "")
        info.save()
        data = {
            "link": reverse("sponsorship_info", args=[event.id, info.url]),
            "url": info.url,
        }
        return Response(data)
    return Response({})


@api_view(['GET'])
def get_mails(request, eventid, mailid, format=None):
    valid, event = event_checker(request, eventid)
    if valid:
        eventmail = EventMail.objects.select_related("Mail").filter(
            event=event, mail__id=mailid).first()
        mails = []
        unread_mails = UnreadMail.objects.filter(
            event=event, user=request.user).values_list(
            "mail__id", flat=True)
        mails.append({
            "username": eventmail.mail.user.username,
            "date": eventmail.mail.date.date(),
            "body": eventmail.mail.body,
            "unread": eventmail.mail.id in unread_mails
        })
        for mail in eventmail.replies.all():
            mails.append({
                "username": mail.user.username,
                "date": mail.date.date(),
                "body": mail.body,
                "unread": mail.id in unread_mails
            })
        unread = UnreadMail.objects.filter(
            user=request.user, event=eventmail.event,
            mail=eventmail.mail).first()
        if unread:
            unread.delete()
        for mail in eventmail.replies.all():
            unread = UnreadMail.objects.filter(
                user=request.user, event=eventmail.event,
                mail=mail).first()
            if unread:
                unread.delete()
        data = {
            "mail_id": eventmail.mail.id,
            "subject": eventmail.mail.subject,
            "mails": mails
        }
        return Response(data)
    return Response({})


@api_view(['GET'])
def get_mails_page(request, eventid, page, format=None):
    """Update mails list."""
    valid, event = event_checker(request, eventid)
    page = int(page) * 10
    event_mails = EventMail.objects.select_related("Mail").filter(
        event=event).order_by("-mail__last_date")
    count = event_mails.count()
    mails = []
    unread_mails = UnreadMail.objects.filter(
        event=event, user=request.user).values_list(
        "mail__id", flat=True)
    for eventmail in event_mails[page:page + 10]:
        username = eventmail.mail.user.username
        replies = eventmail.replies.all().order_by("-date")
        if replies[:1]:
            username = "%s .. %s (%d)" % (
                username, replies[0].user.username, replies.count() + 1)
        mails.append({
            "id": eventmail.mail.id,
            "username": username,
            "subject": eventmail.mail.subject,
            "date": eventmail.mail.date.strftime('%d, %b %Y'),
            "unread": eventmail.mail.id in unread_mails
        })
    if mails:
        page_items = page + 10 if count >= (page + 10) else count
        data = {
            'page': "%d-%d" % (page, page_items),
            'pagenum': count,
            'mails': mails}
        return Response(data)
    return Response({})


@api_view(['GET'])
def get_tasks_page(request, eventid, page, format=None):
    """Update tasks list."""
    valid, event = event_checker(request, eventid)
    page = int(page) * 10
    opened = request.GET.get("opened", "true") == "true"
    filtering = int(request.GET.get("filter", "1"))
    # filtering: 0=EVERYONE, 1=ASSIGNED, 2=CREATED
    if filtering == 2:
        tasks_db = Tasks.objects.filter(
            event=event, active=opened,
            creator=request.user).order_by("priority", "-last_update")
    elif filtering == 1:
        tasks_db = Tasks.objects.filter(
            event=event, active=opened,
            assigned=request.user).order_by("priority", "-last_update")
    else:
        tasks_db = Tasks.objects.filter(
            event=event, active=opened).order_by("priority", "-last_update")
    count = tasks_db.count()
    tasks = []
    for task in tasks_db[page:page + 10]:
        if task.ending:
            date = task.ending.strftime('%d, %b %Y')
            tasks.append({
                "id": task.id,
                "priority": task.priority,
                "date": date,
                "title": task.title,
            })
        else:
            tasks.append({
                "id": task.id,
                "priority": task.priority,
                "title": task.title,
            })
    if tasks:
        page_items = page + 10 if count >= (page + 10) else count
        data = {
            'page': "%d-%d" % (page, page_items),
            'pagenum': count,
            'tasks': tasks}
        return Response(data)
    return Response({})


@api_view(['GET'])
def close_task(request, eventid, taskid, format=None):
    """Close Task."""
    valid, event = event_checker(request, eventid)
    taskid = int(taskid)
    if valid and taskid:
        task = Tasks.objects.filter(
            id=taskid, event=event).first()
        if task:
            task.closed_by = request.user
            task.last_update = datetime.datetime.now()
            task.active = False
            task.save()
            comment = Comments(
                user=request.user, date=datetime.datetime.now(),
                comment="[CLOSED]")
            comment.save()
            task.comments.add(comment)
    return Response({})


@api_view(['GET'])
def get_tasks_data(request, eventid, taskid, format=None):
    """Return task data,"""
    valid, event = event_checker(request, eventid)
    taskid = int(taskid)
    if valid and taskid:
        task = Tasks.objects.filter(
            id=taskid, event=event).first()
        comments = []
        for comment in task.comments.all():
            comments.append({
                "user": comment.user.username,
                "date": comment.date.strftime('%d, %b %Y'),
                "comment": comment.comment
            })
        year = 0
        month = 0
        day = 0
        if task.ending:
            year = task.ending.year
            month = task.ending.month
            day = task.ending.day
        if task:
            data = {
                'title': task.title,
                'year': year,
                'month': month,
                'day': day,
                'priority': task.priority,
                'assigned': task.assigned.username,
                'comments': comments}
            return Response(data)
    return Response({})


@api_view(['GET'])
def get_tasks_dates(request, eventid, format=None):
    valid, event = event_checker(request, eventid)
    tasks_db = Tasks.objects.filter(
        event=event, active=True, ending__isnull=False)
    tasks_dates = []
    for task in tasks_db:
        tasks_dates.append({
            "id": task.id,
            "title": task.title,
            "year": task.ending.year,
            "month": task.ending.month - 1,
            "day": task.ending.day
        })
    if tasks_dates:
        return Response(tasks_dates)
    return Response({})


@api_view(['GET'])
def update_task_date(request, eventid, taskid, format=None):
    taskid = int(taskid)
    year = int(request.GET.get("year", ""))
    month = int(request.GET.get("month", "")) + 1
    day = int(request.GET.get("day", ""))
    valid, event = event_checker(request, eventid)
    if taskid and valid:
        date = datetime.date(year, month, day)
        task = Tasks.objects.filter(
            event=event, id=taskid).first()
        if task:
            task.ending = date
            task.save()
    return Response({})


@api_view(['GET'])
def add_schedule_event(request, eventid, trackid, format=None):
    trackid = int(trackid)
    name = request.GET.get("name", "")
    is_talk = request.GET.get("is_talk", False) == "true"
    start = request.GET.get("start", None)
    end = request.GET.get("end", None)
    if start:
        start = datetime.datetime.fromtimestamp(float(start) / 1000.0)
    if end:
        end = datetime.datetime.fromtimestamp(float(end) / 1000.0)
    valid, event = event_checker(request, eventid)
    if valid:
        track = Track.objects.filter(event=event, id=trackid).first()
        if is_talk:
            talkid = int(request.GET.get("talkid", None))
            talkEvent = TalkEvent.objects.filter(event=event, id=talkid).first()
            schedule, created = Schedule.objects.get_or_create(
                event=event, track=track, talk=talkEvent, activity_name=name,
                start_time=start, end_time=end, is_talk=is_talk)
            if created:
                return Response({"name": name, "itemid": schedule.id})
        else:
            schedule, created = Schedule.objects.get_or_create(
                event=event, track=track, activity_name=name,
                start_time=start, end_time=end, is_talk=is_talk)
            if created:
                return Response({"name": name, "itemid": schedule.id})


@api_view(['GET'])
def update_schedule_event(request, eventid, trackid, format=None):
    trackid = int(trackid)
    start = request.GET.get("start", None)
    end = request.GET.get("end", None)
    itemid = request.GET.get("itemid", None)
    if start:
        start = datetime.datetime.fromtimestamp(float(start) / 1000.0)
    if end:
        end = datetime.datetime.fromtimestamp(float(end) / 1000.0)
    valid, event = event_checker(request, eventid)
    if valid and itemid:
        track = Track.objects.filter(event=event, id=trackid).first()
        schedule = Schedule.objects.filter(
            event=event, track=track, id=itemid).first()
        if schedule:
            schedule.start_time = start
            schedule.end_time = end
            schedule.save()
            return Response({"itemid": schedule.id})


@api_view(['GET'])
def load_track(request, eventid, trackid, format=None):
    trackid = int(trackid)
    valid, event = event_checker(request, eventid)
    if valid:
        track = Track.objects.filter(event=event, id=trackid).first()
        schedule = Schedule.objects.filter(event=event, track=track)
        results = []
        for item in schedule:
            if item.is_talk:
                results.append(
                    {"name": item.talk.talk.title,
                     "start": int(
                         time.mktime(item.start_time.timetuple())) * 1000,
                     "end": int(
                         time.mktime(item.end_time.timetuple())) * 1000,
                     "is_talk": item.is_talk,
                     "itemid": item.id})
            else:
                results.append(
                    {"name": item.activity_name,
                     "start": int(
                         time.mktime(item.start_time.timetuple())) * 1000,
                     "end": int(
                         time.mktime(item.end_time.timetuple())) * 1000,
                     "is_talk": item.is_talk,
                     "itemid": item.id})
        return Response({"data": results})


@api_view(['GET'])
def create_schedule_track(request, eventid, format=None):
    name = request.GET.get("name", "")
    valid, event = event_checker(request, eventid)
    if valid:
        track, _ = Track.objects.get_or_create(event=event, name=name)
    return Response({"name": track.name, "trackid": track.id})


@api_view(['GET'])
def delete_schedule_track(request, eventid, trackid, format=None):
    trackid = int(trackid)
    valid, event = event_checker(request, eventid)
    if valid:
        track = Track.objects.filter(event=event, id=trackid).first()
        if track:
            track.delete()
    return Response({})


@api_view(['GET'])
def remove_schedule_activity(request, eventid, itemid, format=None):
    itemid = int(itemid)
    valid, event = event_checker(request, eventid)
    if valid:
        schedule_item = Schedule.objects.filter(event=event, id=itemid).first()
        if schedule_item:
            schedule_item.delete()
    return Response({"success": True})


@api_view(['POST'])
def create_task_calendar(request, eventid, format=None):
    title = request.POST.get("taskTitleInput", "")
    priority = request.POST.get("priority", "")
    ending = request.POST.get("endingOn", "")
    assigned = request.POST.get("assignedTo", "")
    assigned_user = User.objects.filter(username=assigned).first()
    valid, event = event_checker(request, eventid)
    try:
        if ending and isinstance(ending, basestring):
            date = [int(d) for d in ending.split("/")]
            ending = datetime.date(date[2], date[1] + 1, date[0])
        else:
            ending = None
    except:
        print "not ending assigned"
    if title and assigned_user:
        priority = Tasks.MAP_CHOICES.get(
            priority, Tasks.PR_HIGH)
        task = Tasks(
            creator=request.user, event=event, title=title,
            priority=priority, last_update=datetime.datetime.now(),
            active=True, assigned=assigned_user, ending=ending)
        task.save()
        comment = Comments(
            user=request.user, date=datetime.datetime.now(), comment="")
        comment.save()
        task.comments.add(comment)
        # Once mail has been created, notify other users
        link = reverse("tasks", args=[event.id])
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        r.publish('tasks',
                  json.dumps({"data": {
                              'priority': task.priority,
                              'title': task.title, 'link': link,
                              'user': request.user.username},
                              'notify_users': [assigned_user.username]}))
        return Response({'id': task.id})
    return Response({})


@api_view(['GET'])
def get_unread_mails(request, eventid, format=None):
    valid, event = event_checker(request, eventid)
    if valid:
        unread = UnreadMail.objects.filter(
            event=event, user=request.user).count()
        return Response({'unread': unread})
    return Response({})


@api_view(['GET'])
def get_task_count(request, eventid, format=None):
    valid, event = event_checker(request, eventid)
    if valid:
        count = Tasks.objects.filter(
            event=event, active=True).count()
        return Response({'pending_tasks': count})
    return Response({})


@api_view(['GET'])
def set_configuration(request, eventid, key, format=None):
    valid, event = event_checker(request, eventid)
    if valid:
        value = request.GET["value"] == "true"
        setattr(event.configuration, key, value)
        event.configuration.save()
    return Response({})


@api_view(['GET'])
def get_tags(request, format=None):
    tags = Tag.objects.all()
    if tags:
        return Response({"tags": [tag.name for tag in tags]})
    return Response({})


@api_view(['GET'])
def set_social(request, eventid, format=None):
    valid, event = event_checker(request, eventid)
    if valid:
        social_type = "%s_link" % request.GET.get("type")
        link = request.GET.get("link", "")
        if social_type:
            social, _ = Social.objects.get_or_create(event=event)
            if social:
                setattr(social, social_type, link)
                social.save()
    return Response({})


@api_view(['GET'])
def save_single_page(request, eventid, format=None):
    valid, event = event_checker(request, eventid)
    if valid:
        category = request.GET.get("category")
        text = request.GET.get("text", "")
        if category:
            page, _ = SinglePage.objects.get_or_create(
                event=event, category=category)
            if page:
                page.text = text
                page.save()
    return Response({})


@api_view(['GET'])
def custom_home_order(request, eventid, format=None):
    valid, event = event_checker(request, eventid)
    if valid:
        order = request.GET.get("order", "")
        if order:
            sections, _ = SectionsOrder.objects.get_or_create(event=event)
            if sections:
                sections.order = order
                sections.save()
    return Response({})


@api_view(['GET'])
def remove_home_section(request, eventid, sectionid, format=None):
    """Remove section."""
    valid, event = event_checker(request, eventid)
    #sectionid = int(sectionid)
    if valid and sectionid:
        order = SectionsOrder.objects.filter(event=event).first()
        if order:
            new_order = []
            for num in order.order.split(","):
                if num != sectionid:
                    new_order.append(num)
            new_order = ",".join(new_order)
            order.order = new_order
            order.save()

            section = HomeSections.objects.filter(
                id=int(sectionid), event=event).first()
            if section:
                section.delete()
    return Response({})


@api_view(['GET'])
def get_home_section(request, eventid, sectionid, format=None):
    """Get section."""
    valid, event = event_checker(request, eventid)
    sectionid = int(sectionid)
    if valid and sectionid:
        section = HomeSections.objects.filter(
            id=sectionid, event=event).first()
        if section:
            data = {
                "sectionid": section.id,
                "title": section.title,
                "text": section.text,
            }
            return Response(data)
    return Response({})


class LocationDetails(APIView):

    def patch(self, request, format=None):
        #FIXME: Not being used it seems
        #locationid = self.kwargs['locationid']
        #location = Location.objects.get(
            #id=locationid, speaker=request.user)
        #serializer = LocationSerializer(location, instance=location)
        #FIXME: undefined status
        #return Response(status=status.HTTP_201_CREATED)
        return Response({})


class TalkDetails(APIView):

    def get(self, request, talkid, format=None):
        talk = Talk.objects.get(id=talkid, speaker=request.user)
        serializer = TalkSerializer(talk)
        return Response(serializer.data)