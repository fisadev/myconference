#!/bin/bash
set -e
LOGFILE=/var/log/myconference/django.log
LOGDIR=$(dirname $LOGFILE)

NUM_WORKERS=3
TIMEOUT_WORKERS=120  #in seconds

BIND_DIR=127.0.0.1:4000

# user/group to run as
USER=www-data
GROUP=www-data
cd /vol/myconference
source /vol/myconference_env/bin/activate
test -d $LOGDIR || mkdir -p $LOGDIR
exec nice --2 gunicorn_django -w $NUM_WORKERS -t $TIMEOUT_WORKERS -b $BIND_DIR \
    --user=$USER --group=$GROUP --log-level=debug \
        --log-file=$LOGFILE 2>>$LOGFILE
