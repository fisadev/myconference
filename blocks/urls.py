from django.conf.urls import patterns, include


urlpatterns = patterns(
    '',

    # VIEWS
    (r'^register/', include('blocks.register.urls')),
    (r'^talks/', include('blocks.talks.urls')),
    (r'^schedule/', include('blocks.schedule.urls')),
    (r'^attendees/', include('blocks.attendees.urls')),
    (r'^profile/', include('blocks.attendees.urls')),
    (r'^single_pages/', include('blocks.single_pages.urls')),
)
