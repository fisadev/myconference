from django.conf.urls import patterns, url

from blocks.talks import api_views


urlpatterns = patterns(
    '',
    # API
    url(r'^remove/(?P<talkid>\d+)/$', api_views.remove_talk_event),
    url(r'^speaker/add/(?P<talkid>\d+)/$', api_views.add_speaker),
    url(r'^(?P<talkid>\d+)/vote/$', api_views.vote_talk),
    url(r'^(?P<eventid>\d+)/configuration/set/publish/$',
        api_views.publish_talk_results),
    url(r'^tags/$', api_views.get_tags),
)
