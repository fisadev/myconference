from django.db import models

from eventmanager.models import Event


class SinglePage(models.Model):
    event = models.ForeignKey(Event)
    category = models.CharField(max_length=100)
    text = models.TextField()

    class Meta:
        unique_together = ('event', 'category',)