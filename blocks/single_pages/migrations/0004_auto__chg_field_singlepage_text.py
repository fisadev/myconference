# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'SinglePage.text'
        db.alter_column(u'single_pages_singlepage', 'text', self.gf('django.db.models.fields.TextField')())

    def backwards(self, orm):

        # Changing field 'SinglePage.text'
        db.alter_column(u'single_pages_singlepage', 'text', self.gf('django.db.models.fields.TextField')(max_length=5000000))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'eventmanager.configuration': {
            'Meta': {'object_name': 'Configuration'},
            'block_attendees': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_call_for_proposals': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_code_of_conduct': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block_contact': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_information': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_my_talks': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_press_release': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block_register': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_review_talks': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_schedule': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_sponsors': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_submit_talk': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'block_talk_page': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block_volunteers': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'first_time': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'publish_talk_results': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'site_template': ('django.db.models.fields.CharField', [], {'default': "'design02'", 'max_length': '50'})
        },
        u'eventmanager.event': {
            'Meta': {'object_name': 'Event'},
            'admin': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_admin'", 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'configuration': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.Configuration']"}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_member'", 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '1000'}),
            'timezone': ('timezone_field.fields.TimeZoneField', [], {'default': "'UTC'"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '150'})
        },
        u'single_pages.singlepage': {
            'Meta': {'unique_together': "(('event', 'category'),)", 'object_name': 'SinglePage'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['eventmanager.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['single_pages']