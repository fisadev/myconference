from PIL import Image
from django import forms
from django.forms import ValidationError
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Div, Layout
from .models import SponsorLevel, Sponsor


class SponsorLevelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SponsorLevelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'col-md-5'
        self.helper.form_id = 'level-form'
        self.helper.add_input(Submit('sponsorlevel_submit', _('Submit')))

    class Meta:
        model = SponsorLevel
        exclude = ('event',)


class SponsorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SponsorForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('sponsor_submit', _('Submit')))
        self.helper.form_id = 'sponsor-form'
        self.helper.layout = Layout(
            Div(
                'level',
                'name',
                'logo',
                'email',
                css_class='column col-md-4'
            ),
            Div(
                'phone',
                'company_page',
                'description',
                css_class='column col-md-4'
            ),
        )

    class Meta:
        model = Sponsor
        exclude = ('event',)

    def clean_logo(self):
        image = self.cleaned_data.get('logo', False)

        if image:
            img = Image.open(image)
            w, h = img.size
            max_width = 900
            max_height = 900

            #if image._size > 1*1024*1024:
                #raise ValidationError(_("Image file too big (maximum 1MB)"))
            if w > max_width or h > max_height:
                raise ValidationError(
                    _("Image dimensions too big (maximum 900x900 pixels)"))
        return image