from django.db import models
from django.utils.translation import ugettext_lazy as _

from eventmanager.models import Event


class SponsorLevel(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('Name'))
    amount = models.IntegerField(default=0,
                                 verbose_name=_('Amount'),
        help_text=_('''Amount of sponsors allowed in this level.
                    Default is 0 for unlimited amount.'''))
    importance = models.IntegerField(default=0,
                                     verbose_name=_('Importance'),
                help_text=_('''Level of importance, lower numbers
                            mean more importance.'''))
    event = models.ForeignKey(Event, verbose_name=_('Event'))

    class Meta:
        ordering = ['importance', ]

    def __unicode__(self):
        return self.name


class Sponsor(models.Model):
    level = models.ForeignKey(
        SponsorLevel, verbose_name=_('Level'), blank=True, null=True)
    event = models.ForeignKey(Event, verbose_name=_('Event'))
    name = models.CharField(max_length=100, verbose_name=_('Name'))
    email = models.EmailField(max_length=100)
    logo = models.ImageField(upload_to='images', blank=True, null=True)
    phone = models.CharField(max_length=100, verbose_name=_('Phone'),
                             null=True, blank=True)
    company_page = models.URLField(max_length=200, verbose_name=_('Website'))
    description = models.CharField(max_length=500,
                                   verbose_name=_('Description'), null=True, blank=True)

    class Meta:
        unique_together = ('name', 'event',)


class SponsorshipInfo(models.Model):
    event = models.ForeignKey(Event, unique=True)
    url = models.CharField(max_length=300)