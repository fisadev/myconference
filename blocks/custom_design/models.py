from django.db import models
from eventmanager.models import Event
from django.utils.translation import ugettext_lazy as _


class CustomDesign(models.Model):
    event = models.ForeignKey(Event, verbose_name=_('Event'))
    bg_image = models.ImageField(upload_to='custom', blank=True, null=True,
                                 verbose_name=_('Background image'))
    about_us_image = models.ImageField(upload_to='custom',
                                       blank=True, null=True)
    custom_css = models.TextField(blank=True, null=True,
                                  verbose_name=_('Custom CSS'))


class HomeSections(models.Model):
    event = models.ForeignKey(Event)
    title = models.CharField(max_length=100)
    text = models.TextField()


class SectionsOrder(models.Model):
    event = models.ForeignKey(Event, unique=True)
    order = models.CharField(max_length=1000)