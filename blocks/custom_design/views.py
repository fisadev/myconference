from django.shortcuts import render
from .models import CustomDesign
from .forms import DesignForm
from eventmanager.api.api_views import is_valid_event_and_info
from django.contrib.auth.decorators import login_required
from pyconference.common.utils import render_response
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


@login_required
@is_valid_event_and_info
def edit_design(request, eventid=0, **kw):
    event = kw["event"]
    d, created = CustomDesign.objects.get_or_create(event=event)
    unread = kw["unread"]
    tasks = kw["tasks"]
    messages = kw["messages"]
    newer_tasks = kw["newer_tasks"]
    data = {
        'project_name': event.title,
        'ui_designer_active': 'active',
        'unread': unread,
        'tasks': tasks,
        'eventid': eventid,
        'messages': messages,
        'newer_tasks': newer_tasks
    }
    if request.method == 'POST':
        form = DesignForm(request.POST, request.FILES, instance=d)
        if form.is_valid:
            if created:
                f = form.save(commit=False)
                f.event = event
                f.save()
            else:
                form.save()
    form = DesignForm(instance=d)
    data['form'] = form
    return render_response(request, 'custom_design.html', data)
