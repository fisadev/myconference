from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField

from eventmanager.models import Event


class Profile(models.Model):
    user = models.OneToOneField(User)
    country = CountryField(verbose_name=_('country'))
    state = models.CharField(max_length=100, verbose_name=_('state'))
    phone = models.CharField(
        max_length=100, verbose_name=_('phone'), null=True, blank=True)
    personal_page = models.CharField(
        max_length=200, verbose_name=_('personal page'), null=True, blank=True)
    company = models.CharField(
        max_length=100, verbose_name=_('company'), null=True, blank=True)
    company_page = models.CharField(
        max_length=200, verbose_name=_('company page'), null=True, blank=True)
    biography = models.TextField(
        verbose_name=_('biography'), null=True, blank=True)
    show_email = models.BooleanField(
        verbose_name=_('display e-mail publically'), default=False)
    show_real_name = models.BooleanField(
        verbose_name=_('show real name'), default=True)
    public = models.BooleanField(
        default=True, verbose_name=_('public profile'))
    in_attendees = models.BooleanField(
        default=True, verbose_name=_('include in attendees lists'))
    cv_link = models.URLField(verbose_name=_('CV link'), null=True, blank=True)
    cv = models.FileField(upload_to='cv', verbose_name='CV file', null=True,
                          blank=True)
    allow_contact = models.BooleanField(
        default=True, verbose_name=_('allow contact from sponsors'))


class Attendee(models.Model):
    profile = models.ForeignKey(Profile)
    event = models.ForeignKey(Event)

    class Meta:
        unique_together = ('profile', 'event',)
