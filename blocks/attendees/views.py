from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils import translation
from eventmanager.forms import ProfileForm, UserForm
from pyconference.common.utils import render_response

from eventmanager.models import Event
from blocks.views import event_context
from blocks.attendees.models import Profile


@event_context
def attendees(request, url, **kw):
    if not kw["context"]["block_attendees"]:
        raise Http404
    event = kw["event"]
    context = kw['context']
    attendees = event.attendee_set.all()
    context['attendees'] = attendees
    context["conference_active"] = "active"
    translation.activate(context['language'])
    return render_response(request, 'attendees.html', context)


def profile(request, url='', username=''):
    user = request.user

    is_conference_site = False
    if url != 'dashboard':
        is_conference_site = True
    if request.method == 'GET':
        if user.is_authenticated() and (
                not username or user.username == username):
            profile, created = Profile.objects.get_or_create(user=user)
            user_form = UserForm(instance=user)
            profile_form = ProfileForm(instance=profile)
            context = dict(user=user, profile_form=profile_form,
                           user_form=user_form, dashboard=True)
            if is_conference_site:
                return HttpResponseRedirect(reverse('block_register',
                                            args=[url]))
            else:
                return render_response(request, 'profile_edit.html', context)
        elif not user.is_authenticated() and not username:
            raise Http404
        else:
            user = get_object_or_404(User, username=username)
            profile = get_object_or_404(Profile, user=user)
            if not profile.public:
                raise Http404
            context = dict(user=user, profile=profile, dashboard=True)
            event = Event.objects.filter(url=url).first()
            if event:
                translation.activate(event.language)
            if is_conference_site:
                context["url"] = url
                return render_response(
                    request,
                    'conference/design02/conference_profile.html',
                    context)
            else:
                return render_response(request, 'profile_view.html', context)

    elif request.method == 'POST':
        if user.is_authenticated():
            profile, created = Profile.objects.get_or_create(user=user)
            user_form = UserForm(request.POST, request.FILES, instance=user)
            profile_form = ProfileForm(request.POST, request.FILES, instance=profile)
            if user_form.is_valid() and profile_form.is_valid():
                user_form.save()
                profile.user = user
                if (profile.personal_page and
                        not profile.personal_page.startswith("http://")):
                    profile.personal_page = "http://%s" % profile.personal_page
                if (profile.company_page and
                        not profile.company_page.startswith("http://")):
                    profile.company_page = "http://%s" % profile.company_page
                profile.save()
            else:
                if user.is_authenticated() and (
                        not username or user.username == username):
                    profile, created = Profile.objects.get_or_create(user=user)
                    user_form = UserForm(instance=user)
                    profile_form = ProfileForm(instance=profile)
                    context = dict(user=user, profile_form=profile_form,
                                   user_form=user_form, dashboard=True)
                    context["error"] = True
                    return render_response(request, 'profile_edit.html',
                                           context)
        return HttpResponseRedirect(reverse('profile', args=[
            url, user.username]))