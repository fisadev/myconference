import datetime
import pytz

from django.http import Http404
from django.utils import translation
from django.template.defaultfilters import date as _date
from django.core.urlresolvers import reverse

from eventmanager.models import Event
from blocks.attendees.models import Attendee
from blocks.social.models import Social
from blocks.sponsors.models import SponsorLevel
from blocks.custom_design.models import CustomDesign


def event_context(func):
    def wrapper(request, url, **kw):
        event = Event.objects.filter(url=url).first()
        if event:
            login_url = reverse("account_login")
            if "pycon.com.ar" in request.build_absolute_uri() or \
                    "ar.pycon.org" in request.build_absolute_uri():
                login_url = "http://myconference.co/accounts/login/"
            if not request.user.is_anonymous():
                user_registered = (Attendee.objects.filter(
                    profile__user=request.user,
                    event=event).first() is not None)
            else:
                user_registered = False
            if event.configuration.private and not user_registered:
                raise Http404
            expired = event.end_date < datetime.datetime.now(
                pytz.timezone(str(event.timezone)))
            social, _ = Social.objects.get_or_create(event=event)
            try:
                custom_design = CustomDesign.objects.get(event=event)
            except CustomDesign.DoesNotExist:
                custom_design = None
            translation.activate(event.language)
            sponsors_level = SponsorLevel.objects.filter(event=event)
            sponsors_db = event.sponsor_set.all()
            sponsors = []
            if sponsors_db.count() > 0:
                for level in sponsors_level:
                    if level.amount < 5 and level.amount > 0:
                        size = (100 / level.amount) - 5
                    else:
                        size = 15
                    sponsors.append({"level": level.name, "sponsors": [],
                                     "size": size})
                    for sponsor in sponsors_db:
                        if sponsor.level == level:
                            sponsors[-1]["sponsors"].append(sponsor)
            design, created = CustomDesign.objects.get_or_create(event=event)
            custom_css = ""
            if (not created and design.custom_css and
                    "</style>" not in design.custom_css):
                custom_css = design.custom_css
            context = dict(eventid=event.id,
                           event=event,
                           conference_name=event.title,
                           url=event.url,
                           description=event.summary,
                           start_date=_date(event.start_date, "D d b"),
                           end_date=_date(event.end_date, "D d b"),
                           custom_css_style=custom_css,
                           expired=expired,
                           custom_design=custom_design,
                           login_url=login_url,
                           user_registered=user_registered,
                           twitter_link=social.twitter_link,
                           facebook_link=social.facebook_link,
                           google_plus_link=social.google_link,
                           pinterest_link=social.pinterest_link,
                           sponsors=sponsors,
                           language=event.language)
            if (request.user in event.admin.all() or
                    request.user in event.members.all() or
                    request.user in event.reviewers.all()):
                context["is_organizer"] = True
            else:
                context["is_organizer"] = False
            load_configuration(context, event)
            kw["event"] = event
            kw["context"] = context
            return func(request, url, **kw)
        else:
            raise Http404
    return wrapper


def load_configuration(context, event):
    # CHECK CONFIGURATIONS
    attributes = event.configuration.attributes_names
    for attribute in attributes:
        context[attribute] = getattr(event.configuration, attribute)
    context["publish_talk_results"] = event.configuration.publish_talk_results
    context["design"] = event.configuration.site_template
    context["base_template"] = "conference/%s/base.html" % context['design']
    # END CHECK CONFIGURATIONS
