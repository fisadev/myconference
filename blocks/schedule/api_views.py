from rest_framework.decorators import api_view
from rest_framework.response import Response

from blocks.schedule.models import Schedule, ScheduleFavorites


@api_view(['GET'])
def add_favorite(request, scheduleid, format=None):
    scheduleid = int(scheduleid)
    schedule = Schedule.objects.filter(id=scheduleid).first()
    favorite, created = ScheduleFavorites.objects.get_or_create(
        schedule=schedule, user=request.user)
    if created:
        return Response({"added": True})
    return Response({})


@api_view(['GET'])
def remove_favorite(request, scheduleid, format=None):
    scheduleid = int(scheduleid)
    schedule = Schedule.objects.filter(id=scheduleid).first()
    favorite = ScheduleFavorites.objects.filter(
        schedule=schedule, user=request.user).first()
    if favorite:
        favorite.delete()
        return Response({"removed": True})
    return Response({})