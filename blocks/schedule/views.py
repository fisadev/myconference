import datetime

#from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect
from django.template.defaultfilters import date as _date
from django.utils import translation
from django.core.urlresolvers import reverse

from pyconference.common.utils import render_response
from blocks.schedule.models import Track, Schedule, ScheduleFavorites
from blocks.attendees.models import Profile

from blocks.views import event_context


@event_context
def schedule(request, url, event, **kw):
    if not kw["context"]["block_schedule"]:
        raise Http404
    context = kw["context"]
    context["conference_active"] = "active"
    translation.activate(context['language'])

    schedules = []

    dates = []
    date = event.start_date
    delta = datetime.timedelta(days=1)
    while date <= event.end_date:
        dates.append(date)
        date += delta

    track_names = []

    tracks_db = Track.objects.filter(event=event).order_by("name")
    for track in tracks_db:
        track_names.append(track.name)
    for date in dates:
        day_data = {}
        day_data['date'] = _date(date, "D d b")
        talks = []
        temp_schedule = {}
        for i, track in enumerate(tracks_db):
            track_order = "track%d" % i
            schedule_db = Schedule.objects.filter(
                event=event, track=track, start_time__year=date.year,
                start_time__month=date.month,
                start_time__day=date.day).order_by("start_time")
            for schedule in schedule_db:
                key = schedule.start_time.strftime("%H%M")
                info = {}
                info['scheduleid'] = schedule.id
                info['start_time'] = schedule.start_time
                info['end_time'] = schedule.end_time
                info['is_talk'] = schedule.is_talk
                if schedule.is_talk:
                    info['title'] = schedule.talk.talk.title
                    info['talkid'] = schedule.talk.id
                    info['abstract'] = schedule.talk.talk.summary

                    user = schedule.talk.talk.speaker
                    info['user'] = user
                    profile = Profile.objects.filter(user=user).first()
                    info['speaker'] = "%s %s" % (
                        user.first_name, user.last_name)
                    info['speaker_web'] = profile.personal_page
                    info['extra_speakers'] = \
                        schedule.talk.talk.extra_speakers.all()
                    info['company'] = profile.company
                    info['company_web'] = profile.company_page

                    if not request.user.is_anonymous():
                        favorite = ScheduleFavorites.objects.filter(
                            user=request.user, schedule=schedule).first()
                        if favorite:
                            info['favorite'] = True
                        else:
                            info['favorite'] = False
                else:
                    info['title'] = schedule.activity_name

                obj = temp_schedule.get(key, {})
                obj[track_order] = info
                temp_schedule[key] = obj
        talks = [temp_schedule[item] for item in sorted(temp_schedule)]
        day_data["talks"] = talks
        schedules.append(day_data)

    context["schedules"] = schedules
    context["track_names"] = track_names
    return render_response(request, 'schedule.html', context)


@event_context
def schedule_favorites(request, url, event, **kw):
    if not kw["context"]["block_schedule"]:
        raise Http404
    if request.user.is_anonymous():
        return HttpResponseRedirect(reverse('block_schedule',
                                            args=[url]))
    context = kw["context"]
    context["conference_active"] = "active"
    translation.activate(context['language'])

    dates = []
    date = event.start_date
    delta = datetime.timedelta(days=1)
    while date <= event.end_date:
        dates.append(date)
        date += delta

    favorites = ScheduleFavorites.objects.filter(
        user=request.user).values_list("schedule", flat=True)
    favorites_schedule = []
    for date in dates:
        date_data = {}
        date_data['date'] = _date(date, "D d b")
        schedules = []
        schedule_db = Schedule.objects.filter(
            event=event, start_time__year=date.year,
            start_time__month=date.month,
            start_time__day=date.day).order_by("start_time")
        for schedule in schedule_db:
            if schedule.id in favorites:
                info = {}
                info['scheduleid'] = schedule.id
                info['trackname'] = schedule.track.name
                info['start_time'] = schedule.start_time
                info['end_time'] = schedule.end_time
                info['title'] = schedule.talk.talk.title
                info['talkid'] = schedule.talk.id
                info['abstract'] = schedule.talk.talk.summary

                user = schedule.talk.talk.speaker
                info['user'] = user
                profile = Profile.objects.filter(user=user).first()
                info['speaker'] = "%s %s" % (
                    user.first_name, user.last_name)
                info['speaker_web'] = profile.personal_page
                info['company'] = profile.company
                info['company_web'] = profile.company_page

                favorite = ScheduleFavorites.objects.filter(
                    user=request.user, schedule=schedule).first()
                if favorite:
                    info['favorite'] = True
                else:
                        info['favorite'] = False
                schedules.append(info)
        date_data["schedules"] = schedules
        favorites_schedule.append(date_data)

    context["favorites"] = favorites_schedule
    return render_response(request, 'schedule_favorites.html', context)