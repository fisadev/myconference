from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect
from django.utils import translation

from pyconference.common.utils import render_response

from eventmanager.forms import ProfileForm, UserForm
from blocks.attendees.models import Attendee, Profile

from blocks.views import event_context


@login_required
@event_context
def register(request, url, **kw):
    """Show register to conference page."""
    if not kw["context"]["block_register"]:
        raise Http404
    event = kw["event"]
    context = kw["context"]
    # Profile Info
    user = request.user
    translation.activate(context['language'])

    if request.method == 'GET':
        profile, created = Profile.objects.get_or_create(user=user)
        user_form = UserForm(instance=user)
        profile_form = ProfileForm(instance=profile)
        local_context = dict(user=user, profile_form=profile_form,
                             user_form=user_form, register_active="active")
        context.update(local_context)
        return render_response(request, 'register.html', context)
    elif request.method == 'POST':
        profile, created = Profile.objects.get_or_create(user=user)
        user_form = UserForm(request.POST, instance=user)
        profile_form = ProfileForm(request.POST, instance=profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile.user = user
            if (profile.personal_page and
                    not profile.personal_page.startswith("http://")):
                profile.personal_page = "http://%s" % profile.personal_page
            if (profile.company_page and
                    not profile.company_page.startswith("http://")):
                profile.company_page = "http://%s" % profile.company_page
            profile.save()
            attendee, _ = Attendee.objects.get_or_create(
                profile=Profile.objects.get(user=user), event=event)
        return HttpResponseRedirect(reverse('show_conference',
                                            args=[event.url]))
