from django.conf.urls import patterns, url
from blocks.location import views

urlpatterns = patterns(
    url(r'^locations/(?P<eventid>\d+)/$', views.locations, name='locations'),
)