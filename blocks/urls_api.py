from django.conf.urls import patterns, url, include

from blocks import api_views

urlpatterns = patterns(
    '',

    # VIEWS
    #(r'^register/', include('blocks.register.urls')),
    (r'^talks/', include('blocks.talks.urls_api')),
    (r'^schedule/', include('blocks.schedule.urls_api')),
    url(r'^usernames/$', api_views.get_usernames),
)
