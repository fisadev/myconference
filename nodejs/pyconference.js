var http = require('http');
var server = http.createServer().listen(4000);
var io = require('socket.io').listen(server);
var cookie_reader = require('cookie');
var querystring = require('querystring');

var redis = require('socket.io/node_modules/redis');
var sub = redis.createClient();

var userToSocket = {};
var socketToUser = {};

//Subscribe to the Redis chat channel
sub.subscribe('mails');
sub.subscribe('tasks');

// Configure socket.io to store cookie set by Django
io.configure(function(){
    io.set('authorization', function(data, accept){
        if(data.headers.cookie){
            data.cookie = cookie_reader.parse(data.headers.cookie);
            return accept(null, true);
        }
        return accept('error', false);
    });
    io.set('log level', 1);
});

//Grab message from Redis and send to client
sub.on('message', function(channel, message){
  console.log(channel);
  var data = JSON.parse(message);
  users = data.notify_users;
  for (var i=0; i < users.length; i++) {
    var socketid = userToSocket[users[i]];
    io.sockets.socket(socketid).emit(channel, JSON.stringify(data.data));
  }
});

io.sockets.on('connection', function (socket) {

    socket.on("register", function(data) {
      userToSocket[data.username] = socket.id;
      socketToUser[socket.id] = data.username;
      console.log("register");
    });

    socket.on('disconnect', function () {
      var username = socketToUser[socket.id];
      delete userToSocket[username];
      delete socketToUser[socket.id];
      console.log("quit");
    });

    //Client is sending message through socket.io
    // socket.on('send_mail', function (data) {
    //     console.log(data);
    //     values = querystring.stringify({
    //         comment: data.my,
    //         sessionid: socket.handshake.cookie['sessionid'],
    //     });
    //
    //     var options = {
    //         host: 'localhost',
    //         port: 8000,
    //         path: '/rtapi/send_mail2',
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/x-www-form-urlencoded',
    //             'Content-Length': values.length
    //         }
    //     };
    //
    //     //Send message to Django server
    //     var req = http.request(options, function(res){
    //         res.setEncoding('utf8');
    //
    //         //Print out error message
    //         res.on('data', function(message){
    //             if(message != 'true'){
    //                 console.log('Message: ' + message);
    //             }
    //         });
    //     });
    //
    //     req.write(values);
    //     req.end();
    // });
});
