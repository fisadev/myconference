API_DASHBOARD = "http://myconference.co/dashboard/api/"

function buildUri() {
  uri = API_DASHBOARD;
  for (var i = 0; i < arguments.length; i++) {
    uri += arguments[i] + "/";
  }

  return uri;
}
