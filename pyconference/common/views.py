from pyconference.common.utils import render_response
#from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
#from django.utils import simplejson
#from django.contrib.admin.views.decorators import staff_member_required
#from django.views.decorators.http import require_POST
from django.utils import translation
from blocks.views import event_context

from blocks.custom_design.models import HomeSections, SectionsOrder


def home(request):
    """Home Page."""
    if "pycon.com.ar" in request.build_absolute_uri() or \
            "ar.pycon.org" in request.build_absolute_uri():
        return HttpResponseRedirect(reverse("show_conference",
                                    args=["pyconar2014"]))
    data = {}
    return render_response(request, 'index.html', data)


def error_404(request):
    data = {}
    return render_response(request, 'error404.html', data)


@event_context
def show_conference(request, url, event, context):
    """Get the proper Conference from the DB and render the site."""
    context.update({"home_active": "active"})
    lang = context['language']
    translation.activate(lang)

    # Adding custom sections
    sections = HomeSections.objects.filter(event=event)
    sections_order = SectionsOrder.objects.filter(event=event).first()
    orders = []
    if sections_order:
        sort_by = sections_order.order.split(",")
        for sectionid in sort_by:
            for sect in sections:
                if sectionid != "" and sect.id == int(sectionid):
                    orders.append(sect)
                    break
    else:
        for sect in sections:
            orders.append(sect)

    for sect in sections:
        if sect not in orders:
            orders.append(sect)
    if orders:
        context["sections"] = orders

    return render_response(request,
                           'conference/%s/index.html' % context['design'],
                           context)