var form_validated = false;

function addWebsiteUrlCallback($textInput, callback, delay) {
  var timer = null;
  $textInput.keyup(function() {
    if (timer) {
        window.clearTimeout(timer);
    }
    timer = window.setTimeout( function() {
        timer = null;
        callback();
    }, delay );
  });
  $textInput = null;
}

function checkValidConferenceUrl() {
  var $url = $('#id_event-url').val();
  $.get(buildUri("valid_conference_url"),
  { eventurl: $url})
  .done(function( data ) {
    if (data.available) {
      form_validated = true;
      $('#id_event-url').css("color", "black");
      $('#id_event-url').css("background", "#ffffff");
      var url = "http://myconference.co/" + $('#id_event-url').val();
      $('#myConfUrl').text(url);
      $('#myConfUrl').attr("href", url);
    } else {
      form_validated = false;
      $('#id_event-url').css("color", "black");
      $('#id_event-url').css("background", "#ffc4c4");
    }
  });
}

addWebsiteUrlCallback($("#id_event-url"), checkValidConferenceUrl, 500);

$(document).ready(function() {
  $("#id_event-tags").select2({
      tags: []
  });

  $.get(buildUri("event", "tags"))
  .done(function( data ) {
    if (data.tags) {
      $("#id_event-tags").select2({
          tags: data.tags
      });
    }
  });

});
