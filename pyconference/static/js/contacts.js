$(document).ready(function() {
  $('.table').dataTable();

  $.get(buildUri("usernames"), {})
  .done(function(data) {
    $( "#usernameMember" ).autocomplete({
      source: data,
      appendTo: $("#myModalMember")
    });
    $( "#usernameAdmin" ).autocomplete({
      source: data,
      appendTo: $("#myModalAdmin")
    });
    $( "#usernameReviewer" ).autocomplete({
      source: data,
      appendTo: $("#myModalReviewer")
    });
  });
} );

function addReviewer() {
  var eventid = $("#header").attr("name");
  var username = $("#usernameReviewer").val();
  $.get(buildUri(eventid, "reviewer", "add"),
  { username: username})
  .done(function( data ) {
      if (data.username) {
        var body = $("#body_members");
        var content = "<tr>"
                    + "<td><strong>" + data.username + "</strong></td>"
                    + "<td><strong>" + data.first_name + "</strong></td>"
                    + "<td><strong>" + data.last_name + "</strong></td>"
                    + "<td>" + data.email + "</td>"
                    + "<td><a href=\"" + data.webpage + "\">" + data.webpage + "</a></td>"
                    + "<td>" + data.telephone + "</td>"
                    + "</tr>";
        body.append(content);
      }
  });
  $("#myModalReviewer").modal('hide');
}

function addMember() {
  var eventid = $("#header").attr("name");
  var username = $("#usernameMember").val();
  $.get(buildUri(eventid, "member", "add"),
  { username: username})
  .done(function( data ) {
      if (data.username) {
        var body = $("#body_members");
        var content = "<tr>"
                    + "<td><strong>" + data.username + "</strong></td>"
                    + "<td><strong>" + data.first_name + "</strong></td>"
                    + "<td><strong>" + data.last_name + "</strong></td>"
                    + "<td>" + data.email + "</td>"
                    + "<td><a href=\"" + data.webpage + "\">" + data.webpage + "</a></td>"
                    + "<td>" + data.telephone + "</td>"
                    + "</tr>";
        body.append(content);
      }
  });
  $("#myModalMember").modal('hide');
}

function addAdmin() {
  var eventid = $("#header").attr("name");
  var username = $("#usernameAdmin").val();
  $.get(buildUri(eventid, "admin", "add"),
  { username: username})
  .done(function( data ) {
      if (data.username) {
        var body = $("#body_members");
        var content = "<tr>"
                    + "<td><strong>" + data.username + "</strong></td>"
                    + "<td><strong>" + data.first_name + "</strong></td>"
                    + "<td><strong>" + data.last_name + "</strong></td>"
                    + "<td>" + data.email + "</td>"
                    + "<td><a href=\"" + data.webpage + "\">" + data.webpage + "</a></td>"
                    + "<td>" + data.telephone + "</td>"
                    + "</tr>";
        body.append(content);
      }
  });
  $("#myModalAdmin").modal('hide');
}

function removeContact(username, action) {
  $("#userToRemove").text(username);
  $("#remove").val(username);
  $("#operation").val(action);
  $("#myModalRemove").modal('show');
}

function generateSponsorsView() {
  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "sponsors", "info", "generate"))
  .done(function( data ) {
      if (data.link) {
        var $linkObject = $("#linkSponsorsView");
        $linkObject.attr("href", data.link);
        $linkObject.text(data.url);
      }
  });
}
