var opts = {
  lines: 11, // The number of lines to draw
  length: 20, // The length of each line
  width: 10, // The line thickness
  radius: 30, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: '50%', // Top position relative to parent
  left: '50%' // Left position relative to parent
};

var trackActive = -1;
var currentEvent = null;

var idCounter = 1;

function showCreateTrack() {
  var $modal = $("#create-track-modal");
  $modal.modal('show');
}

function showCreateActivity(start, end) {
  if (start) {
    var $startingOn = $("#startingOn");
    var hours = start.getHours();
    if (hours < 10)
      hours = "0" + hours;
    var minutes = start.getMinutes();
    if (minutes < 10)
      minutes = "0" + minutes;
    $startingOn.val(hours + ":" + minutes);
  }
  if (end) {
    var $endingOn = $("#endingOn");
    var hours = end.getHours();
    if (hours < 10)
      hours = "0" + hours;
    var minutes = end.getMinutes();
    if (minutes < 10)
      minutes = "0" + minutes;
    $endingOn.val(hours + ":" + minutes);
  }
  var $modal = $("#create-activity-modal");
  $modal.modal('show');
}

function createActivity() {
  if (trackActive == -1) {
    alert("Track not selected.");
    return;
  }
  var $activityName = $("#activityName");
  var name = $activityName.val();
  var start = $("#startingOn").val().split(":");
  var end = $("#endingOn").val().split(":");
  $activityName.val("");
  $("#startingOn").val("--:--");
  $("#endingOn").val("--:--");

  var date = $('#calendar').fullCalendar('getDate');
  var startDate = new XDate(date.getFullYear(), date.getMonth(), date.getDate(), start[0], start[1], true);
  date.setHours(start[0]);
  date.setMinutes(start[1]);
  var dateFinish = $('#calendar').fullCalendar('getDate');
  var endDate = new XDate(dateFinish.getFullYear(), dateFinish.getMonth(), dateFinish.getDate(), end[0], end[1], true);
  dateFinish.setHours(end[0]);
  dateFinish.setMinutes(end[1]);

  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "schedule", "add_event", trackActive),
  {name: name,
   is_talk: false,
   start: startDate.getTime(),
   end: endDate.getTime()})
  .done(function(data) {
    if (data.name) {
      $('#calendar').fullCalendar('addEventSource',
        [{title: data.name, id: idCounter, start: date, end: dateFinish, allDay: false, is_talk: false, itemid: data.itemid}]);
        idCounter += 1;
    }
  });
}

function assignTalk(eventObject) {
  if (trackActive == -1) {
    alert("Track not selected.");
    return;
  }

  var date = eventObject.start;
  var startDate = new XDate(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), true);
  var dateFinish = eventObject.end;
  var endDate = new XDate(dateFinish.getFullYear(), dateFinish.getMonth(), dateFinish.getDate(), dateFinish.getHours(), dateFinish.getMinutes(), true);

  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "schedule", "add_event", trackActive),
  {name: eventObject.title,
   is_talk: true,
   talkid: eventObject.itemid,
   start: startDate.getTime(),
   end: endDate.getTime()})
  .done(function(data) {
    if (data.name) {
      $('#calendar').fullCalendar('addEventSource',
        [{title: data.name, id: idCounter, start: date, end: dateFinish, allDay: false, is_talk: true, itemid: data.itemid}]);
        idCounter += 1;
    }
  });
}

function updateActivity(start, end) {
  if (trackActive == -1) {
    alert("Track not selected.");
    return;
  }

  if (start == null) {
    var start = $("#eventStartingOn").val().split(":");
    var date = $('#calendar').fullCalendar('getDate');
    date.setHours(start[0]);
    date.setMinutes(start[1]);
  } else {
    var date = start;
  }
  if (end == null) {
    var end = $("#eventEndingOn").val().split(":");
    var dateFinish = $('#calendar').fullCalendar('getDate');
    dateFinish.setHours(end[0]);
    dateFinish.setMinutes(end[1]);
  } else {
    var dateFinish = end;
  }
  var startDate = new XDate(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), true);
  var endDate = new XDate(dateFinish.getFullYear(), dateFinish.getMonth(), dateFinish.getDate(), dateFinish.getHours(), dateFinish.getMinutes(), true);
  $("#startingOn").val("--:--");
  $("#endingOn").val("--:--");

  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "schedule", "update_event", trackActive),
  {itemid: currentEvent.itemid,
   start: startDate.getTime(),
   end: endDate.getTime()})
  .done(function(data) {
    if (data.itemid) {
      currentEvent.start = date;
      currentEvent.end = dateFinish;
      $('#calendar').fullCalendar('updateEvent', currentEvent);
    }
  });
}

function createTrack() {
  var $trackName = $("#trackName");
  var name = $trackName.val();
  $trackName.val("");

  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "schedule", "create", "track"),
  {name: name})
  .done(function(data) {
    if (data.name) {
        var $accordion = $(".panel-group");
        var index = $accordion.children().length + 1;
        var content = '<div class="panel" id="panel' + index + '"><div class="panel-heading">' +
                '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse' + index + '" id="track' + data.trackid + '">' + data.name + '</a></div>' +
            '<div id="collapse' + index + '" class="panel-collapse collapse" style="height: auto;">' +
                '<div class="panel-body">' +
                  '<button onclick="setTrackActive(' + data.trackid + ')" id="create-track" class="btn btn-success">Set active</button>' +
                  '<button onclick="deleteTrack(' + data.trackid + ',' + index + ')" id="create-track" class="btn btn-danger">Remove</button>' +
                '</div></div></div>';
        $accordion.append(content);
    }
  });
}

function setTrackActive(trackid) {
  var $trackName = $("#track" + trackid);
  var $trackActiveTitle = $("#activeTrackTitle");
  trackActive = trackid;
  $trackActiveTitle.html($trackName.text());
  $('#calendar').fullCalendar('removeEvents')

  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "schedule", "load_track", trackActive))
  .done(function(data) {
    if (data.data) {
      data.data.forEach(function(entry) {
        var date = new XDate(entry.start, true);
        var dateFinish = new XDate(entry.end, true);
        var startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes());
        var endDate = new Date(dateFinish.getFullYear(), dateFinish.getMonth(), dateFinish.getDate(), dateFinish.getHours(), dateFinish.getMinutes());
        $('#calendar').fullCalendar('addEventSource',
          [{title: entry.name, id: idCounter, start: startDate, end: endDate, allDay: false, is_talk: entry.is_talk, itemid: entry.itemid}]);
          idCounter += 1;
      });
    }
  });
}

function deleteTrack(trackid, panel) {
  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "schedule", "delete", "track", trackid))
  .done(function(data) {
    var $panelComponent = $("#panel" + panel);
    $panelComponent.remove();
  });
}

function removeActivity() {
  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "schedule", "remove", "activity", currentEvent.itemid))
  .done(function(data) {
    if (data.success) {
      $('#calendar').fullCalendar('removeEvents', currentEvent.id);
    }
  });
}

function showSpinner() {
  var div = document.getElementById('loading');
  var spinner = new Spinner(opts).spin(div);
}

function hideSpinner(page) {
  var div = $('#loading');
  div.hide();
}

$(function(){

    $('#external-events').find('div.external-event').each(function() {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()), // use the element's text as the event title
            id: $.trim($(this).find("i").attr("id"))
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });

    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
        allDaySlot: false,
        defaultView: 'agendaDay',
        slotDuration: "00:10:00",
        slotEventOverlap: false,

        selectable: true,
        selectHelper: true,
        editable: true,
        droppable:true,
        timezone: "UTC",

        select: function(start, end, allDay) {
            showCreateActivity(start, end);
        },

        eventResize: function(event, delta, revertFunc) {
          currentEvent = event;
          updateActivity(event.start, event.end);
        },

        eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {
          currentEvent = event;
          updateActivity(event.start, event.end);
        },

        drop: function(date, allDay) { // this function is called when something is dropped
            // retrieve the dropped element's stored Event Object
            var eventObject = $(this).data('eventObject');
            var talkid = $(this).attr("id");

            // assign it the date that was reported
            eventObject.start = date;
            var end = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours() + 2, date.getMinutes());
            eventObject.end = end;
            eventObject.allDay = false;
            eventObject.itemid = talkid;
            var result = assignTalk(eventObject);

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            // $('#calendar').fullCalendar('renderEvent', eventObject, true);
            $(this).remove();
        },

        eventClick: function(event) {
          currentEvent = event;
          var $modal = $("#edit-modal");
          $modal.find(".modal-title").html(event.title);

          var start = event.start
          var $startingOn = $("#eventStartingOn");
          var hours = start.getHours();
          if (hours < 10)
            hours = "0" + hours;
          var minutes = start.getMinutes();
          if (minutes < 10)
            minutes = "0" + minutes;
          $startingOn.val(hours + ":" + minutes);
          var end = event.end
          var $endingOn = $("#eventEndingOn");
          var hours = end.getHours();
          if (hours < 10)
            hours = "0" + hours;
          var minutes = end.getMinutes();
          if (minutes < 10)
            minutes = "0" + minutes;
          $endingOn.val(hours + ":" + minutes);
          $modal.modal('show');
        }
    });
});
