$("#id_tags").select2({
    tags: []
});

$.get(buildUri("event", "tags"))
.done(function( data ) {
  if (data.tags) {
    $("#id_tags").select2({
        tags: data.tags
    });
  }
});
