var current_page = 0;
var opened = true;
// filter: 0=EVERYONE, 1=ASSIGNED, 2=CREATED
var filter = 0
var opts = {
  lines: 11, // The number of lines to draw
  length: 20, // The length of each line
  width: 10, // The line thickness
  radius: 30, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: '50%', // Top position relative to parent
  left: '50%' // Left position relative to parent
};

var priority = "high";

function updateTasksListOnNotification() {
  if (current_page == 0 && opened && filter == 0) {
    _loadPage(0, false);
  }
}

callbackTasks = updateTasksListOnNotification;

function showCreateTask() {
  $("#showTasksList").hide();
  var tasksReadArea = $("#tasksRead");
  $(".bodyComments").hide();
  tasksReadArea.find("#taskTitleInput").val("");
  tasksReadArea.find("#taskTitleInput").show();
  tasksReadArea.find("#taskTitle").hide();
  tasksReadArea.find("#tasksBody").val("");
  tasksReadArea.find("#taskId").val("");
  tasksReadArea.show();
}

function showEditTask(taskid) {
  showSpinner();
  $("#showTasksList").hide();
  var tasksReadArea = $("#tasksRead");
  tasksReadArea.find("#taskTitleInput").val("");
  tasksReadArea.find("#taskTitleInput").hide();
  tasksReadArea.find("#taskTitle").text("");
  tasksReadArea.find("#taskTitle").show();
  tasksReadArea.find("#tasksBody").val("");
  tasksReadArea.find("#taskId").val(taskid);
  tasksReadArea.show();
  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "task", "read", taskid))
  .done(function( data ) {
      if (data.title) {
        tasksReadArea.find("#taskTitle").html('<i class="fa fa-check-square-o"></i>' + data.title);
        tasksReadArea.find("#taskTitleInput").val(data.title);
        if (data.priority == 3) {
          $("#optionHigh").attr("class", "btn btn-inverse btn-sm");
          $("#optionMedium").attr("class", "btn btn-inverse btn-sm");
          $("#optionLow").attr("class", "btn btn-success btn-sm active");
        } else if (data.priority == 2) {
          $("#optionHigh").attr("class", "btn btn-inverse btn-sm");
          $("#optionMedium").attr("class", "btn btn-warning btn-sm active");
          $("#optionLow").attr("class", "btn btn-inverse btn-sm");
        } else {
          $("#optionHigh").attr("class", "btn btn-danger btn-sm active");
          $("#optionMedium").attr("class", "btn btn-inverse btn-sm");
          $("#optionLow").attr("class", "btn btn-inverse btn-sm");
        }
        if (data.year) {
          $("#endingOn").val(data.day + "/" + data.month + "/" + data.year);
        }
        $("#assignedName").text(data.assigned);
        comments = data.comments;
        var read = $("#taskRead").clone();
        if (comments.length > 0) {
          $("#accordion").html("");
          $(".bodyComments").show();
          read.find("#readUsername").text(comments[0].user + " (" + comments[0].date + ")");
          read.find("#readBody").html(comments[0].comment.replace(/\r?\n/g,'<br/>'));
          read.appendTo( "#accordion" );
        }
        for (var i = 1; i < comments.length; i++) {
          newRead = read.clone();
          newRead.find("#readUsername").text(comments[i].user + " (" + comments[i].date + ")");
          newRead.find("#readBody").html(comments[i].comment.replace(/\r?\n/g,'<br/>'));
          newRead.find("#readUsername").attr("href", "#collapseItem" + i);
          newRead.find("#collapseItem").attr("id", "collapseItem" + i);
          newRead.appendTo( "#accordion" );
        }
      }
      hideSpinner();
  });
}

function showTasksList() {
  $("#tasksRead").hide();
  $("#showTasksList").show();
}

function closeTask(taskid) {
  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "task", "close", taskid));
}

function createTask() {
  showSpinner();
  $("#assignedTo").val($("#assignedName").text());
  $("#priority").val(priority);
  $("#createTask").submit();
}

function showSpinner() {
  var div = document.getElementById('loading');
  var spinner = new Spinner(opts).spin(div);
}

function hideSpinner(page) {
  var div = $('#loading');
  div.hide();
}

function changeCurrentUser(username) {
  $("#assignedName").text(username);
}

function changePriority(value) {
  priority = value;
}

function loadPage(previous, removeItems) {
  showSpinner();
  page = current_page + 1
  if (previous) {
    page = current_page - 1;
  }
  if (page < 0) {
    return;
  }
  _loadPage(page, removeItems);
}

function _loadPage(page, removeItems) {
  var eventid = $("#header").attr("name");
  $.get(buildUri(eventid, "task", "page", page),
  { opened: opened,
    filter: filter})
  .done(function( data ) {
      if (removeItems) {
        $("#tasksList").html("");
      }
      if (data.tasks && data.tasks.length > 0) {
        $("#tasksList").html("");
        current_page = page;
        tasks = data.tasks;
        $("#pagePositon").text(data.page + " of " + data.pagenum);
        for (var i = 0; i < tasks.length; i++) {
          var taskid = tasks[i].id;
          var title = tasks[i].title;
          var date = tasks[i].date;
          var icon = '<i style="color: red;" class="fa fa-bell"></i>';
          if (tasks[i].priority == 2) {
            icon = '<i style="color: yellow;" class="fa fa-exclamation-circle"></i>';
          } else if (tasks[i].priority == 3) {
            icon = '<i style="color: lightgreen;" class="fa fa-tag"></i>';
          }
          var dateValue = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
          if (tasks[i].date) {
            dateValue = '&nbsp;&nbsp;&nbsp; ' + tasks[i].date + ' &nbsp;&nbsp;&nbsp;';
          }
          taskItem = '<li class="list-group-item">'
                   + icon
                   + '<a class="close" data-dismiss="alert" href="#" onclick="closeTask(' + taskid + ');" aria-hidden="true">&times;</a>'
                   + dateValue
                   + '<a href="#" onclick="showEditTask(' + taskid + ');">' + title + '</a>'
                   + '</li>';
          $("#tasksList").append(taskItem);
        }
      } else if (current_page == -1) {
        current_page = 0;
      }
      hideSpinner();
  });
}

function filterByEveryone() {
  filter = 0;
  current_page = -1;
  $("#everyoneOption").html("<strong>Everyone's Tasks</strong>");
  $("#assignedOption").html("Assigned to you");
  $("#createdOption").html("Created by you");
  loadPage(false, true);
  showTasksList();
}

function filterByAssigned() {
  filter = 1;
  current_page = -1;
  $("#assignedOption").html("<strong>Assigned to you</strong>");
  $("#everyoneOption").html("Everyone's Tasks");
  $("#createdOption").html("Created by you");
  loadPage(false, true);
  showTasksList();
}

function filterByCreated() {
  filter = 2;
  current_page = -1;
  $("#createdOption").html("<strong>Created by you</strong>");
  $("#assignedOption").html("Assigned to you");
  $("#everyoneOption").html("Everyone's Tasks");
  loadPage(false, true);
  showTasksList();
}

function showOpenTasks() {
  opened = true;
  current_page = -1;
  loadPage(false, true);
}

function showClosedTasks() {
  opened = false;
  current_page = -1;
  loadPage(false, true);
}
