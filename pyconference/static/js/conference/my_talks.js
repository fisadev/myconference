currentTalk = -1;

function removeTalk(talkid) {
  $("#talk" + talkid).remove();
  $.get(buildUri("talks", "remove", talkid));
}

function showAddSpeaker(talkid) {
  currentTalk = talkid;
  var $modal = $("#add-speaker");
  $modal.modal('show');
}

$(document).ready(function() {
  $.get(buildUri("usernames"), {})
  .done(function(data) {
    $( "#userName" ).autocomplete({
      source: data,
      appendTo: $("#add-speaker")
    });
  });
} );

function addSpeaker() {
  var username = $("#userName").val();
  alert(username);
  $.get(buildUri("talks", "speaker", "add", currentTalk),
  { username: username})
  .done(function( data ) {
      if (data.username) {
        var body = $("#extraSpeakers");
        var content = "<li><i class='fa fa-user'></i> " + data.username + "</li>";
        body.append(content);
      }
  });
  $("#add-speaker").modal('hide');
}
