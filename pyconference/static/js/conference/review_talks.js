function publishTalkResults(publish, eventid) {
  $.get(buildUri("talks", eventid, "configuration", "set", "publish"),
  { publish: publish});
}


function filterTalks(tag) {
  $("#talks").show();
  if (tag != "reset-all") {
    $("#talks").not("." + tag).hide();
  }
}
