$(document).ready(function() {
	$('#id_saved_talks').change(function() {
		var val = $(this).val();
		$('ul.select2-choices li:not(:last-child)').remove();

		if (val) {
			$.ajax({
				type: 'GET',
				url: '/dashboard/api/talk/' + $(this).val() + '/',
				success: function(data) {
					$.each(data, function(key, value) {
						if (key === 'tags') {
							$.each(data[key], function(index, value) {
								$('ul.select2-choices').prepend('<li class="select2-search-choice">    <div>' + value + '</div>    <a href="#" onclick="return false;" class="select2-search-choice-close" tabindex="-1"></a></li>');
							});
						}
						$('#id_' + key).val(value);
					});
				}
			});
		}
		else {
			$('form').each (function(){
			  this.reset();
			});
		}
	});

	$("#id_tags").select2({
			tags: []
	});

	$.get(buildUri("talks", "tags"))
	.done(function( data ) {
		if (data.tags) {
			$("#id_tags").select2({
					tags: data.tags
			});
		}
	});
});
